﻿namespace FattalOnTimeDealsStarter.Application.Configurations
{
    public class ConnectionsOptions
    {
        public string DbEntity { get; set; }
    }
}
