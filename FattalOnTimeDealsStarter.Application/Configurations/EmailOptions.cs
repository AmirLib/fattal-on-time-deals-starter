﻿using System.Collections.Generic;

namespace FattalOnTimeDealsStarter.Application.Configurations
{
    public class EmailOptions
    {
        public List<string> ToMailAddresses { get; set; }
        public string EndDealsGroupSubject { get; set; }
        public string FromAddress { get; set; }
        public string FromName { get; set; }
        public string Host { get; set; }
    }
}
