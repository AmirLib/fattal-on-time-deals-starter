﻿using FattalOnTimeDealsStarter.Application.Interfaces;
using FattalOnTimeDealsStarter.Domain.Interfaces;
using FattalOnTimeDealsStarter.Domain.Models;
using System;
using System.Collections.Generic;

namespace FattalOnTimeDealsStarter.Application.Services
{
    public class AutomaticDealsDataService : IAutomaticDealsDataService
    {
        public AutomaticDealsDataService(
            IAutomaticDealsDataRepository automaticDealsDataRepository,
            ICmsRepository cmRepository,
            IEmailService emailService)
        {
            _automaticDealsDataRepository = automaticDealsDataRepository;
            _cmRepository = cmRepository;
            _emailService = emailService;
        }

        #region Fields
        private readonly IAutomaticDealsDataRepository _automaticDealsDataRepository;
        private readonly ICmsRepository _cmRepository;
        private readonly IEmailService _emailService;
        #endregion

        public string BuildDealsKey(DateTime arrivalDate, DateTime deptDate)
        {
            return $"{arrivalDate}@{deptDate}";
        }

        public void FindFinishedGroups(Dictionary<int, Dictionary<string, List<int>>> groups)
        {
            var data = GetAllByNotMailSent();

            data.ForEach(e =>
            {
                var key = BuildDealsKey(e.ArrivalDate, e.DeptDate);

                if (e.AllClosed)
                {
                    Finish(e);

                    e.MailSent = true;
                }
            });
        }

        public List<AutomaticDealsData> GetAllByNotMailSent()
        {
            return _automaticDealsDataRepository.GetAllByNotMailSent();
        }

        public void InsertNewInformation(int hotelId, string key, string priceCode)
        {
            var dates = ParseDealsKey(key);

            _automaticDealsDataRepository.InsertNewInformation(hotelId, dates.Item1, dates.Item2, priceCode);
        }

        public void MarkAllClosed(int hotelId, string key)
        {
            var dates = ParseDealsKey(key);
            var data = _automaticDealsDataRepository.Get(hotelId, dates.Item1, dates.Item2);

            if (!data.AllClosed) data.AllClosed = true;
        }

        public (DateTime, DateTime) ParseDealsKey(string key)
        {
            var dates = key.Split(new char[] { '@' });

            return (DateTime.Parse(dates[0]), DateTime.Parse(dates[1])); 
        }

        private void Finish(AutomaticDealsData data)
        {
            var name = _cmRepository.GetNameBarById(data.HotelId).NameBar;

            SendEndHotelDealsEmail(
                name,
                data.ArrivalDate,
                data.DeptDate,
                data.PriceCode);
        }

        private void SendEndHotelDealsEmail(
            string hotelName,
            DateTime arrivalDate,
            DateTime deptDate,
            string priceCode)
        {
            _emailService.SendEndDealsGroup(
                hotelName,
                arrivalDate,
                deptDate,
                priceCode);
        }
    }
}
