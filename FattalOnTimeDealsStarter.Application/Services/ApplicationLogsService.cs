﻿using FattalOnTimeDealsStarter.Application.Interfaces;
using FattalOnTimeDealsStarter.Domain.Enums;
using FattalOnTimeDealsStarter.Domain.Interfaces;
using FattalOnTimeDealsStarter.Domain.Models;
using System;

namespace FattalOnTimeDealsStarter.Application.Services
{
    public class ApplicationLogsService: IApplicationLogsService
    {
        public ApplicationLogsService(IApplicationLogRepository applicationLogsService)
        {
            _applicationLogsService = applicationLogsService;
        }

        #region Fields
        private readonly IApplicationLogRepository _applicationLogsService;
        #endregion

        public void Add(
            LogLevel level, 
            LogType type,
            int? orderId, 
            string methodName, 
            string message, 
            string stackTrace, 
            string serverIp, 
            string userDevice, 
            string userBrowser)
        {
            _applicationLogsService.Add(
                new ApplicationLog
                {
                    LogLevel = (int)level,
                    DateCreated = DateTime.Now,
                    Type = (int)type,
                    OrderId = orderId,
                    MethodName = methodName,
                    Message = message,
                    StackTrace = stackTrace,
                    ServerIp = serverIp,
                    UserDevice = userDevice,
                    UserBrowser = userBrowser
                });
        }
    }
}
