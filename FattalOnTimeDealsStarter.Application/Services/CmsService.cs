﻿using FattalOnTimeDealsStarter.Application.Interfaces;
using FattalOnTimeDealsStarter.Domain.Interfaces;
using FattalOnTimeDealsStarter.Domain.Models;
using System.Collections.Generic;

namespace FattalOnTimeDealsStarter.Application.Services
{
    public class CmsService : ICmsService
    {
        public CmsService(IAutomaticDealsDataService automaticDealsDataService, ICmsRepository cmsRepository)
        {
            _automaticDealsDataService = automaticDealsDataService;
            _cmsRepository = cmsRepository;
        }

        #region ICmsRepository
        private readonly IAutomaticDealsDataService _automaticDealsDataService;
        private readonly ICmsRepository _cmsRepository;
        #endregion

        public Dictionary<int, Dictionary<string, List<int>>> GetAllActiveGroupedByHotel()
        {
            var deals = _cmsRepository.GetAllActiveAsDictionary();
            var result = new Dictionary<int, Dictionary<string, List<int>>>();

            foreach (var pair in deals)
            {
                var hotelKey = pair.Value.DealHotelId.Value;
                var dealsKey = _automaticDealsDataService.BuildDealsKey(
                    pair.Value.DealArrivalDate.Value,
                    pair.Value.DealDeptDate.Value); // key represents list of deals of the same arrival and dept dates.

                if (result.ContainsKey(hotelKey)) // if hotel key exists
                {
                    if (result[hotelKey].ContainsKey(dealsKey)) // if key exists
                    {
                        result[hotelKey][dealsKey].Add(pair.Value.Id); // add new deal
                    }
                    else
                    {
                        result[hotelKey].Add(dealsKey, new List<int> // add new dictionary item with the new key and new list
                        {
                            pair.Value.Id,
                        });
                    }
                }
                else
                {
                    result.Add(hotelKey, new Dictionary<string, List<int>>());

                    result[hotelKey].Add(dealsKey, new List<int>
                    {
                        pair.Value.Id,
                    });
                }
            }

            return result;
        }

        public Dictionary<int, Cm> GetAllActiveAsDictionary()
        {
            return _cmsRepository.GetAllActiveAsDictionary();
        }

        public void SaveChanges()
        {
            _cmsRepository.SaveChanges();
        }
    }
}
