﻿using FattalOnTimeDealsStarter.Application.Resources;
using FattalOnTimeDealsStarter.Application.Configurations;
using FattalOnTimeDealsStarter.Application.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using FattalOnTimeDealsStarter.Domain.Enums;

namespace FattalOnTimeDealsStarter.Application.Services
{
    public class EmailService: IEmailService
    {
        public EmailService(IConfigurationRoot config, IApplicationLogsService applicationLogService)
        {
            _emailConfiguration = GetEmailConfiguration(config);
            _applicationLogService = applicationLogService;
        }

        #region Fields
        private readonly EmailOptions _emailConfiguration;
        private readonly IApplicationLogsService _applicationLogService;
        #endregion

        public void SendEndDealsGroup(
            string hotelName,
            DateTime arrivalDate,
            DateTime deptDate,
            string priceCode)
        {
            var body = EmailResource.EndDealsGroupBody;

            body = body
                .Replace(EmailResource.ArrivalDate, arrivalDate.ToString("d"))
                .Replace(EmailResource.DeptDate, deptDate.ToString("d"))
                .Replace(EmailResource.PriceCode, priceCode);

            var subject = CreateSubject(hotelName, arrivalDate, priceCode);

            SendMail(
                _emailConfiguration.FromAddress,
                _emailConfiguration.FromName,
                _emailConfiguration.ToMailAddresses,
                null,
                subject,
                body.Replace(EmailResource.HotelName, hotelName),
                true);
        }

        #region Private Methods
        private string CreateSubject(string hotelName, DateTime arrivalDate, string priceCode)
        {
            var subject = _emailConfiguration.EndDealsGroupSubject;

            return subject
                .Replace(EmailResource.HotelName, hotelName)
                .Replace(EmailResource.ArrivalDate, arrivalDate.ToString("d"))
                .Replace(EmailResource.PriceCode, priceCode);
        }

        private EmailOptions GetEmailConfiguration(IConfigurationRoot config)
        {
            return config
                .GetSection("EmailConfiguration")
                .Get<EmailOptions>();
        }

        private void SendMail(
            string fromEmail,
            string fromName,
            List<string> toAddresses,
            List<string> ccAddresses,
            string subject,
            string body,
            bool isBodyHTML = false)
        {
            try
            {
                var host = _emailConfiguration.Host;
                var fromMailAddress = new MailAddress(fromEmail, fromName);
                var mail = new MailMessage()
                {
                    Body = body,
                    From = fromMailAddress,
                    IsBodyHtml = isBodyHTML,
                    Subject = subject
                };
                var smtp = new SmtpClient
                {
                    EnableSsl = false,
                    Host = host,
                    Port = 25,
                };

                toAddresses.ForEach(address => mail.To.Add(address));
                ccAddresses?.ForEach(address => mail.CC.Add(address));
                smtp.Send(mail);
            }
            catch (Exception e)
            {
                _applicationLogService.Add(
                    LogLevel.Error,
                    LogType.Email,
                    null,
                    "SendMail",
                    e.Message,
                    null,
                    GeneralResource.ServiceName,
                    null,
                    null);
            }
        }
        #endregion
    }
}
