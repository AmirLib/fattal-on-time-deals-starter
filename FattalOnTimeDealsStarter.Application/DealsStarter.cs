﻿using FattalOnTimeDealsStarter.Application.Interfaces;
using FattalOnTimeDealsStarter.Domain.Interfaces;
using FattalOnTimeDealsStarter.Domain.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FattalOnTimeDealsStarter.Application
{
    public class DealsStarter
    {
        public DealsStarter(
            IAutomaticDealsDataService automaticDealsDataService,
            IConfigurationRoot config,
            ICmsService cmsService,
            IDealHostBaseAndPriceCodeRepository dealHostBaseAndPriceCodeRepository)
        {
            _automaticDealsDataService = automaticDealsDataService;
            _config = config;
            _cmsService = cmsService;
            _dealHostBaseAndPriceCodeRepository = dealHostBaseAndPriceCodeRepository;
        }

        #region Fields
        private readonly IAutomaticDealsDataService _automaticDealsDataService;
        private readonly ICmsService _cmsService;
        private readonly IConfigurationRoot _config;
        private readonly IDealHostBaseAndPriceCodeRepository _dealHostBaseAndPriceCodeRepository;
        #endregion

        public void Run()
        {
            var deals = _cmsService.GetAllActiveAsDictionary();
            var dealsGroupByHotel = _cmsService.GetAllActiveGroupedByHotel();

            foreach (var dealsByHotel in dealsGroupByHotel)
            {
                foreach (var pair in dealsByHotel.Value)
                {
                    var dealsSet = pair.Value.ToHashSet();
                    var orderedDealsByPresentage = _dealHostBaseAndPriceCodeRepository.GetDealsByPrecentageGroups(dealsSet);
                    var priceCode = orderedDealsByPresentage
                        .SelectMany(group => group)
                        .FirstOrDefault();

                    _automaticDealsDataService.InsertNewInformation(dealsByHotel.Key, pair.Key, priceCode.PriceCode.ParentPriceCode);
                    DeactivateDeals(deals, orderedDealsByPresentage);

                    if (AnyActivePrecentageGroupDeals(deals, orderedDealsByPresentage)) continue;

                    var dealsNeedToActive = GetDealIdsToActivate(deals, orderedDealsByPresentage);

                    if (dealsNeedToActive is not null)
                    {
                        ActivateDeals(deals, dealsNeedToActive);
                    }
                    else
                    {
                        _automaticDealsDataService.MarkAllClosed(dealsByHotel.Key, pair.Key);
                    }

                    _cmsService.SaveChanges();
                }
            }

            _automaticDealsDataService.FindFinishedGroups(dealsGroupByHotel);
            _cmsService.SaveChanges();
        }

        #region Private Methods
        private void ActivateDeals(Dictionary<int, Cm> deals, List<int> ids)
        {
            ids.ForEach(id =>
            {
                if (deals[id].DealStartDate <= DateTime.Now)
                    deals[id].Show = true; 
            });
        }

        private bool AnyActivePrecentageGroupDeals(
            Dictionary<int, Cm> deals,
            List<IGrouping<int, DealHostBaseAndPriceCode>> groups)
        {
            return groups.Any(group => group.Any(d =>
            {
                var deal = deals[d.DealId];

                return deal.DealAvailableQuantity > 0 && deal.Show.Value;
            }));
        }

        private void DeactivateDeals(
            Dictionary<int, Cm> deals,
            List<IGrouping<int, DealHostBaseAndPriceCode>> groups)
        {
            groups
                .ForEach(group => group
                    .Where(d => deals[d.DealId].DealAvailableQuantity == 0 && deals[d.DealId].Show.Value)
                    .ToList()
                    .ForEach(d =>
                    {
                        deals[d.DealId].DealEndDate = DateTime.Now;
                        deals[d.DealId].Show = false;
                    })
               );
        }

        private List<int> GetDealIdsToActivate(
            Dictionary<int, Cm> deals,
            List<IGrouping<int, DealHostBaseAndPriceCode>> groups)
        {
            var group = groups
                .FirstOrDefault(group => group.Any(d =>
                {
                    var deal = deals[d.DealId];

                    return deal.DealTotalQuantity == deal.DealAvailableQuantity
                        && deal.DealAvailableQuantity > 0
                        && deal.Show.HasValue
                        && !deal.Show.Value;
                }));

            return group is not null ?
                group
                    .Select(item => item.DealId)
                    .ToList() :
                null;
        }
        #endregion
    }
}
