﻿using FattalOnTimeDealsStarter.Domain.Models;
using System.Collections.Generic;

namespace FattalOnTimeDealsStarter.Application.Interfaces
{
    public interface ICmsService
    {
        Dictionary<int, Dictionary<string, List<int>>> GetAllActiveGroupedByHotel();
        Dictionary<int, Cm> GetAllActiveAsDictionary();
        void SaveChanges();
    }
}
