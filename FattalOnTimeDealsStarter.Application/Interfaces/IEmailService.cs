﻿using System;

namespace FattalOnTimeDealsStarter.Application.Interfaces
{
    public interface IEmailService
    {
        void SendEndDealsGroup(
            string hotelName,
            DateTime arrivalDate,
            DateTime deptDate,
            string priceCode);
    }
}
