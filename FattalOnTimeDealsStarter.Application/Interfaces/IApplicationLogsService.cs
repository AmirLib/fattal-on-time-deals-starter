﻿using FattalOnTimeDealsStarter.Domain.Enums;

namespace FattalOnTimeDealsStarter.Application.Interfaces
{
    public interface IApplicationLogsService
    {
        void Add(
            LogLevel level,
            LogType type,
            int? orderId,
            string methodName,
            string message,
            string stackTrace,
            string serverIp,
            string userDevice,
            string userBrowse);
    }
}
