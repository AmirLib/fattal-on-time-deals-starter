﻿using FattalOnTimeDealsStarter.Domain.Models;
using System;
using System.Collections.Generic;

namespace FattalOnTimeDealsStarter.Application.Interfaces
{
    public interface IAutomaticDealsDataService
    {
        string BuildDealsKey(DateTime arrivalDate, DateTime deptDate);
        void FindFinishedGroups(Dictionary<int, Dictionary<string, List<int>>> groups);
        List<AutomaticDealsData> GetAllByNotMailSent();
        void InsertNewInformation(int hotelId, string key, string priceCode);
        void MarkAllClosed(int hotelId, string key);
        (DateTime, DateTime) ParseDealsKey(string key);
    }
}
