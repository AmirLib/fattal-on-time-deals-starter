﻿using FattalOnTimeDealsStarter.Domain.Models;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace FattalOnTimeDealsStarter.Infra.Data
{
    public partial class FattalNewContext : DbContext
    {
        public FattalNewContext() { }

        public FattalNewContext(DbContextOptions<FattalNewContext> options) : base(options) { }

        public virtual DbSet<AdmAction> AdmActions { get; set; }
        public virtual DbSet<AdmUser> AdmUsers { get; set; }
        public virtual DbSet<AdminUserOrderAction> AdminUserOrderActions { get; set; }
        public virtual DbSet<ApplicationLog> ApplicationLogs { get; set; }
        public virtual DbSet<ApplicationLogsContact> ApplicationLogsContacts { get; set; }
        public virtual DbSet<ApplicationLogsHelper> ApplicationLogsHelpers { get; set; }
        public virtual DbSet<AutomaticDealsData> AutomaticDealsData { get; set; }
        public virtual DbSet<AutomaticDealsGroup> AutomaticDealsGroups { get; set; }
        public virtual DbSet<CancelationOrder> CancelationOrders { get; set; }
        public virtual DbSet<CartLine> CartLines { get; set; }
        public virtual DbSet<Cm> Cms { get; set; }
        public virtual DbSet<CmsGallery> CmsGalleries { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<ContactU> ContactUs { get; set; }
        public virtual DbSet<Content> Contents { get; set; }
        public virtual DbSet<ContentBody> ContentBodies { get; set; }
        public virtual DbSet<Coupon> Coupons { get; set; }
        public virtual DbSet<CurrenciesPelecardCode> CurrenciesPelecardCodes { get; set; }
        public virtual DbSet<Currency> Currencies { get; set; }
        public virtual DbSet<DealAvailabilty> DealAvailabilties { get; set; }
        public virtual DbSet<DealAvalabilty> DealAvalabilties { get; set; }
        public virtual DbSet<DealHostBaseAndPriceCode> DealHostBaseAndPriceCodes { get; set; }
        public virtual DbSet<DivurUser> DivurUsers { get; set; }
        public virtual DbSet<Favorite> Favorites { get; set; }
        public virtual DbSet<G4ToGocAndSkyGoc> G4ToGocAndSkyGocs { get; set; }
        public virtual DbSet<HostBase> HostBases { get; set; }
        public virtual DbSet<HotelAvailableHostBase> HotelAvailableHostBases { get; set; }
        public virtual DbSet<HotelCompositionOptima> HotelCompositionOptimas { get; set; }
        public virtual DbSet<HotelPriceCodeAndPercentage> HotelPriceCodeAndPercentages { get; set; }
        public virtual DbSet<HotelRoomFeature> HotelRoomFeatures { get; set; }
        public virtual DbSet<Log> Logs { get; set; }
        public virtual DbSet<Module> Modules { get; set; }
        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<OptimaG4hotelsPriceCodeAndId> OptimaG4hotelsPriceCodeAndIds { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrdersSpecialRequest> OrdersSpecialRequests { get; set; }
        public virtual DbSet<ParametersTbl> ParametersTbls { get; set; }
        public virtual DbSet<PaymentServiceLog> PaymentServiceLogs { get; set; }
        public virtual DbSet<PaypalHotel> PaypalHotels { get; set; }
        public virtual DbSet<PelecardHotel> PelecardHotels { get; set; }
        public virtual DbSet<PelecardSucceededOrder> PelecardSucceededOrders { get; set; }
        public virtual DbSet<PlazmaCounter> PlazmaCounters { get; set; }
        public virtual DbSet<Redirect> Redirects { get; set; }
        public virtual DbSet<Room> Rooms { get; set; }
        public virtual DbSet<SkyClubMember> SkyClubMembers { get; set; }
        public virtual DbSet<SkyStatus> SkyStatuses { get; set; }
        public virtual DbSet<SpecialRequest> SpecialRequests { get; set; }
        public virtual DbSet<TempCm> TempCms { get; set; }
        public virtual DbSet<UniqIpOrder> UniqIpOrders { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserMessage> UserMessages { get; set; }
        public virtual DbSet<UsersChild> UsersChilds { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Hebrew_CI_AS");

            modelBuilder.Entity<AdmAction>(entity =>
            {
                entity.HasNoKey();
                entity.ToTable("admActions");

                entity.Property(e => e.ActionDate)
                    .HasColumnType("datetime")
                    .HasColumnName("actionDate");

                entity.Property(e => e.ActionObjectId).HasColumnName("actionObjectId");

                entity.Property(e => e.ActionTable)
                    .HasMaxLength(50)
                    .HasColumnName("actionTable");

                entity.Property(e => e.ActionType)
                    .HasMaxLength(50)
                    .HasColumnName("actionType");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(e => e.UserName)
                    .HasMaxLength(100)
                    .HasColumnName("userName");
            });

            modelBuilder.Entity<AdmUser>(entity =>
            {
                entity.ToTable("admUsers");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("email");

                entity.Property(e => e.Idate)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("idate");

                entity.Property(e => e.Job)
                    .HasMaxLength(50)
                    .HasColumnName("job");

                entity.Property(e => e.LastLogindate)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("lastLogindate");

                entity.Property(e => e.Mobile)
                    .HasMaxLength(16)
                    .IsUnicode(false)
                    .HasColumnName("mobile");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .HasColumnName("name");

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .HasColumnName("password");

                entity.Property(e => e.Phone)
                    .HasMaxLength(16)
                    .IsUnicode(false)
                    .HasColumnName("phone");

                entity.Property(e => e.Prio).HasColumnName("prio");

                entity.Property(e => e.Show).HasColumnName("show");

                entity.Property(e => e.StatusAuth).HasColumnName("statusAuth");

                entity.Property(e => e.Udate)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("udate");

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .HasColumnName("username");
            });

            modelBuilder.Entity<AdminUserOrderAction>(entity =>
            {
                entity.ToTable("adminUserOrderAction");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ActionDate)
                    .HasColumnType("datetime")
                    .HasColumnName("actionDate");

                entity.Property(e => e.ActionType)
                    .HasMaxLength(150)
                    .HasColumnName("actionType");

                entity.Property(e => e.OrderId).HasColumnName("orderId");

                entity.Property(e => e.UserName)
                    .HasMaxLength(150)
                    .HasColumnName("userName");
            });

            modelBuilder.Entity<ApplicationLog>(entity =>
            {
                entity.HasIndex(e => new { e.Type, e.LogLevel, e.DateCreated }, "IDX_ApplicationLogs_type_Type_LogLevel_DateCreated");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.ServerIp).HasMaxLength(50);

                entity.Property(e => e.UserBrowser).HasMaxLength(50);

                entity.Property(e => e.UserDevice).HasMaxLength(50);
            });

            modelBuilder.Entity<ApplicationLogsHelper>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ApplicationLogsHelper");

                entity.HasIndex(e => e.Type, "IDX_ApplicationLogsHelper_type");

                entity.Property(e => e.ExecutionCount).HasColumnName("executionCount");

                entity.Property(e => e.IsActive).HasColumnName("isActive");

                entity.Property(e => e.IsIncludeData).HasColumnName("isIncludeData");

                entity.Property(e => e.LastExecution)
                    .HasColumnType("datetime")
                    .HasColumnName("lastExecution");

                entity.Property(e => e.LastSent)
                    .HasColumnType("datetime")
                    .HasColumnName("lastSent");

                entity.Property(e => e.Message).HasColumnName("message");

                entity.Property(e => e.MessageSubject).HasColumnName("messageSubject");

                entity.Property(e => e.NumberOfErrorsToNotify).HasColumnName("numberOfErrorsToNotify");

                entity.Property(e => e.NumberOfNotificationsBeforeStopNotify).HasColumnName("numberOfNotificationsBeforeStopNotify");

                entity.Property(e => e.Query).HasColumnName("query");

                entity.Property(e => e.TimeFromStopNotifyToReturnNotifyInMinutes).HasColumnName("timeFromStopNotifyToReturnNotifyInMinutes");

                entity.Property(e => e.TimeIntervalBetweenCheckes).HasColumnName("timeIntervalBetweenCheckes");

                entity.Property(e => e.Type).HasColumnName("type");
            });

            modelBuilder.Entity<CancelationOrder>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("cancelationOrders");

                entity.Property(e => e.Comments).HasMaxLength(256);

                entity.Property(e => e.Date)
                    .HasColumnType("datetime")
                    .HasColumnName("date");

                entity.Property(e => e.Email)
                    .HasMaxLength(64)
                    .HasColumnName("email");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(32)
                    .HasColumnName("firstName");

                entity.Property(e => e.HotelName)
                    .HasMaxLength(32)
                    .HasColumnName("hotelName");

                entity.Property(e => e.IdCustomer)
                    .HasMaxLength(32)
                    .HasColumnName("idCustomer");

                entity.Property(e => e.Lastname)
                    .HasMaxLength(32)
                    .HasColumnName("lastname");

                entity.Property(e => e.OrderId).HasColumnName("orderId");

                entity.Property(e => e.Phone)
                    .HasMaxLength(32)
                    .HasColumnName("phone");
            });

            modelBuilder.Entity<CartLine>(entity =>
            {
                entity.ToTable("cartLines");

                entity.HasIndex(e => e.OrderId, "IDX_cartLines_orderid");

                entity.HasIndex(e => e.CartId, "IX_cartLines_cartId");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BaseId).HasColumnName("baseId");

                entity.Property(e => e.CartId)
                    .HasMaxLength(150)
                    .HasColumnName("cartId");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.LockedFinalPrice).HasColumnName("lockedFinalPrice");

                entity.Property(e => e.OrderId).HasColumnName("orderId");

                entity.Property(e => e.Roomname)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("roomname")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.SkyClubDiscountPrice).HasColumnName("skyClubDiscountPrice");

                entity.Property(e => e.SystemText).HasMaxLength(250);

                entity.Property(e => e.TimeStamp)
                    .HasColumnType("datetime")
                    .HasColumnName("timeStamp");

                entity.Property(e => e.UniqId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("uniqId")
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<Cm>(entity =>
            {
                entity.ToTable("cms");

                entity.HasIndex(e => e.DealHotelId, "IDX_CMS_DealHotelId");

                entity.HasIndex(e => e.DealStatusId, "IDX_CMS_DealStatusId");

                entity.HasIndex(e => e.FatherId, "IDX_CMS_fatherId");

                entity.HasIndex(e => new { e.Show, e.DealAvailableQuantity, e.DealEndDate }, "IDX_CMS_show_DealAvailableQuantity_DealEndDate");

                entity.HasIndex(e => e.TypeId, "IDX_CMS_typeId");

                entity.HasIndex(e => e.DealEndDate, "IDX_cms_DealEndDate");

                entity.HasIndex(e => e.DealStartDate, "IDX_cms_dealstartdate");

                entity.HasIndex(e => e.HotelArea, "IDX_cms_hotelArea");

                entity.HasIndex(e => new { e.TypeId, e.Language, e.DealIsDisplayedOnHomePage }, "IDX_orders_typeId_language_DealIsDisplayedOnHomePage");

                entity.HasIndex(e => e.NameUrl, "IX_cms_nameUrl");

                entity.HasIndex(e => e.TypeId, "IX_cms_typeId");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ActuallyDealEndTime)
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.BannerId1).HasColumnName("bannerId1");

                entity.Property(e => e.BannerId2).HasColumnName("bannerId2");

                entity.Property(e => e.BannerId3).HasColumnName("bannerId3");

                entity.Property(e => e.BannerId4).HasColumnName("bannerId4");

                entity.Property(e => e.CatalogNumber).HasMaxLength(50);

                entity.Property(e => e.CmsLinkId1).HasColumnName("cmsLinkId1");

                entity.Property(e => e.CmsLinkId2).HasColumnName("cmsLinkId2");

                entity.Property(e => e.CmsViews).HasColumnName("cmsViews");

                entity.Property(e => e.DealArrivalDate).HasColumnType("smalldatetime");

                entity.Property(e => e.DealDefaultRoom).HasColumnName("dealDefaultRoom");

                entity.Property(e => e.DealDeptDate).HasColumnType("smalldatetime");

                entity.Property(e => e.DealEndDate).HasColumnType("smalldatetime");

                entity.Property(e => e.DealHb).HasColumnName("dealHB");

                entity.Property(e => e.DealRoomType).HasMaxLength(255);

                entity.Property(e => e.DealShortText).HasMaxLength(250);

                entity.Property(e => e.DealStartDate).HasColumnType("smalldatetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .HasColumnName("description");

                entity.Property(e => e.Descriptionfordeal).HasColumnName("descriptionfordeal");

                entity.Property(e => e.Directions)
                    .IsRequired()
                    .HasColumnName("directions")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.FatherId).HasColumnName("fatherId");

                entity.Property(e => e.Furl).HasColumnName("furl");

                entity.Property(e => e.HotelAddress).HasMaxLength(100);

                entity.Property(e => e.HotelArea)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("hotelArea")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.HotelBenefits)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("hotelBenefits")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.HotelCityId).HasDefaultValueSql("((0))");

                entity.Property(e => e.HotelEmail).HasMaxLength(250);

                entity.Property(e => e.HotelFax)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.HotelHasWelcomeApp).HasDefaultValueSql("((0))");

                entity.Property(e => e.HotelPhone).HasMaxLength(50);

                entity.Property(e => e.HotelTerminalId)
                    .HasMaxLength(10)
                    .IsFixedLength(true);

                entity.Property(e => e.HotelWelcomeAppLink).HasMaxLength(1000);

                entity.Property(e => e.IsBarPrint).HasColumnName("isBarPrint");

                entity.Property(e => e.IsContactUs).HasColumnName("isContactUs");

                entity.Property(e => e.IsDisplayedOnNavBar).HasColumnName("isDisplayedOnNavBar");

                entity.Property(e => e.IsGallery).HasColumnName("isGallery");

                entity.Property(e => e.IsHoliday).HasColumnName("isHoliday");

                entity.Property(e => e.IsInSiteMap).HasColumnName("isInSiteMap");

                entity.Property(e => e.IsLeftNavBar).HasColumnName("isLeftNavBar");

                entity.Property(e => e.IsNightShow).HasColumnName("isNightShow");

                entity.Property(e => e.IsPaused)
                    .HasColumnName("isPaused")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IsRelNofollow).HasColumnName("isRelNofollow");

                entity.Property(e => e.IsRightNavBar).HasColumnName("isRightNavBar");

                entity.Property(e => e.IsSaturday).HasColumnName("isSaturday");

                entity.Property(e => e.IsSecure).HasColumnName("isSecure");

                entity.Property(e => e.IsShop).HasColumnName("isShop");

                entity.Property(e => e.IsShowMoreItemsAfter).HasColumnName("isShowMoreItemsAfter");

                entity.Property(e => e.IsSpecialOccasion).HasColumnName("isSpecialOccasion");

                entity.Property(e => e.IsTalkBack).HasColumnName("isTalkBack");

                entity.Property(e => e.Keywords)
                    .HasMaxLength(500)
                    .HasColumnName("keywords");

                entity.Property(e => e.Language)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("language")
                    .IsFixedLength(true);

                entity.Property(e => e.LinkTo)
                    .HasMaxLength(250)
                    .HasColumnName("linkTo");

                entity.Property(e => e.Map).HasColumnName("map");

                entity.Property(e => e.MapImg)
                    .IsRequired()
                    .HasColumnName("mapImg")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.MapLogo)
                    .HasMaxLength(100)
                    .HasColumnName("mapLogo");

                entity.Property(e => e.Media1Path)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("media1Path");

                entity.Property(e => e.Media1PathAlt)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("media1PathAlt")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Media2Path)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("media2Path");

                entity.Property(e => e.Media2PathAlt)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("media2PathAlt")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Media3Path)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("media3Path");

                entity.Property(e => e.Media3PathAlt)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("media3PathAlt")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Media4Path)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("media4Path");

                entity.Property(e => e.Media4PathAlt)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("media4PathAlt")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Media5Path)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("media5Path");

                entity.Property(e => e.Media5PathAlt)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("media5PathAlt")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Media6Path)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("media6Path");

                entity.Property(e => e.Media6PathAlt)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("media6PathAlt")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Media7Path)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("media7Path");

                entity.Property(e => e.Media7PathAlt)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("media7PathAlt")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.MiscText1)
                    .HasMaxLength(150)
                    .HasColumnName("miscText1");

                entity.Property(e => e.MiscText2)
                    .HasMaxLength(150)
                    .HasColumnName("miscText2");

                entity.Property(e => e.MiscText3)
                    .HasMaxLength(150)
                    .HasColumnName("miscText3");

                entity.Property(e => e.MiscText4)
                    .HasMaxLength(150)
                    .HasColumnName("miscText4");

                entity.Property(e => e.MoreBoxesStyle).HasColumnName("moreBoxesStyle");

                entity.Property(e => e.Motag)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("motag")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .HasColumnName("name");

                entity.Property(e => e.Name2)
                    .HasMaxLength(500)
                    .HasColumnName("name2");

                entity.Property(e => e.NameBar)
                    .HasMaxLength(255)
                    .HasColumnName("nameBar");

                entity.Property(e => e.NameUrl)
                    .HasMaxLength(255)
                    .HasColumnName("nameUrl");

                entity.Property(e => e.NewsLinkId1).HasColumnName("newsLinkId1");

                entity.Property(e => e.OldId).HasColumnName("oldId");

                entity.Property(e => e.OldImageKey)
                    .HasMaxLength(250)
                    .HasColumnName("oldImageKey");

                entity.Property(e => e.Prio).HasColumnName("prio");

                entity.Property(e => e.Robots)
                    .HasMaxLength(55)
                    .IsUnicode(false)
                    .HasColumnName("robots");

                entity.Property(e => e.SDate)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("sDate");

                entity.Property(e => e.SapakId).HasColumnName("sapakId");

                entity.Property(e => e.ShopId).HasColumnName("shopId");

                entity.Property(e => e.ShopPrice).HasColumnName("shopPrice");

                entity.Property(e => e.Show).HasColumnName("show");

                entity.Property(e => e.SpecialOc)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("specialOc")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Title)
                    .HasMaxLength(255)
                    .HasColumnName("title");

                entity.Property(e => e.Titlefordeal).HasColumnName("titlefordeal");

                entity.Property(e => e.Txt).HasColumnName("txt");

                entity.Property(e => e.Txt2).HasColumnName("txt2");

                entity.Property(e => e.Txt3).HasColumnName("txt3");

                entity.Property(e => e.TypeId)
                    .HasMaxLength(50)
                    .HasColumnName("typeId");

                entity.Property(e => e.Url)
                    .HasMaxLength(255)
                    .HasColumnName("url");

                entity.Property(e => e.VicType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("vicType")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ViewsCount).HasColumnName("viewsCount");

                entity.Property(e => e.WebsiteStyle).HasColumnName("websiteStyle");

                entity.Property(e => e.WriterId).HasColumnName("writerId");
            });

            modelBuilder.Entity<CmsGallery>(entity =>
            {
                entity.ToTable("cmsGallery");

                entity.HasIndex(e => e.CmsId, "IDX_cmsGallery_cmsid");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CmsId).HasColumnName("cmsId");

                entity.Property(e => e.Descr)
                    .IsRequired()
                    .HasColumnName("descr")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ImgPath1).HasColumnName("imgPath_1");

                entity.Property(e => e.ImgPath2).HasColumnName("imgPath_2");

                entity.Property(e => e.ImgPath3).HasColumnName("imgPath_3");

                entity.Property(e => e.ImgPath4).HasColumnName("imgPath_4");

                entity.Property(e => e.IsDiploma).HasColumnName("isDiploma");

                entity.Property(e => e.IsEntertainment)
                    .HasColumnName("isEntertainment")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IsGallery)
                    .IsRequired()
                    .HasColumnName("isGallery")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.IsReason)
                    .HasColumnName("isReason")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IsRoom).HasColumnName("isRoom");

                entity.Property(e => e.IsVideo).HasColumnName("isVideo");

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .HasColumnName("name");

                entity.Property(e => e.Prio).HasColumnName("prio");

                entity.Property(e => e.Show).HasColumnName("show");
            });

            modelBuilder.Entity<ContactU>(entity =>
            {
                entity.ToTable("contactUs");

                entity.HasIndex(e => e.Idate, "idx_contactUs_idate");

                entity.HasIndex(e => e.SubjectId, "idx_contactUs_subjectId");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Address)
                    .HasMaxLength(255)
                    .HasColumnName("address");

                entity.Property(e => e.City)
                    .HasMaxLength(100)
                    .HasColumnName("city");

                entity.Property(e => e.ClientIp)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("clientIP");

                entity.Property(e => e.CompanyName)
                    .HasMaxLength(255)
                    .HasColumnName("companyName");

                entity.Property(e => e.Email)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("email");

                entity.Property(e => e.File1)
                    .HasMaxLength(550)
                    .HasColumnName("file_1");

                entity.Property(e => e.FullName)
                    .HasMaxLength(255)
                    .HasColumnName("fullName");

                entity.Property(e => e.Idate)
                    .HasColumnType("datetime")
                    .HasColumnName("idate");

                entity.Property(e => e.InsideTxt)
                    .HasMaxLength(2000)
                    .HasColumnName("insideTxt");

                entity.Property(e => e.IsDivur).HasColumnName("isDivur");

                entity.Property(e => e.IsPlatinum).HasColumnName("isPlatinum");

                entity.Property(e => e.Language)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("language")
                    .IsFixedLength(true);

                entity.Property(e => e.Mobile)
                    .HasMaxLength(16)
                    .IsUnicode(false)
                    .HasColumnName("mobile");

                entity.Property(e => e.Msg)
                    .HasMaxLength(2000)
                    .HasColumnName("msg");

                entity.Property(e => e.Phone)
                    .HasMaxLength(16)
                    .IsUnicode(false)
                    .HasColumnName("phone");

                entity.Property(e => e.SiteUrl)
                    .HasMaxLength(500)
                    .HasColumnName("siteUrl");

                entity.Property(e => e.StatusId).HasColumnName("statusId");

                entity.Property(e => e.SubjectId).HasColumnName("subjectId");

                entity.Property(e => e.UserId).HasColumnName("userId");
            });

            modelBuilder.Entity<Content>(entity =>
            {
                entity.ToTable("Content");

                entity.Property(e => e.SeoDescription)
                    .HasColumnName("SEO_description")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.SeoKeyWords)
                    .HasColumnName("SEO_keyWords")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.SeoLinkType)
                    .HasMaxLength(2000)
                    .HasColumnName("SEO_linkType")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.SeoRobots)
                    .HasMaxLength(2000)
                    .HasColumnName("SEO_robots")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.SeoTitle)
                    .HasMaxLength(1000)
                    .HasColumnName("SEO_title")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Url).IsRequired();

                entity.HasOne(d => d.Section)
                    .WithMany(p => p.Contents)
                    .HasForeignKey(d => d.SectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Content_news");
            });

            modelBuilder.Entity<ContentBody>(entity =>
            {
                entity.ToTable("ContentBody");
            });

            modelBuilder.Entity<Coupon>(entity =>
            {
                entity.Property(e => e.Code).HasMaxLength(50);

                entity.Property(e => e.IsPercent).HasColumnName("isPercent");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Show).HasColumnName("show");
            });

            modelBuilder.Entity<CurrenciesPelecardCode>(entity =>
            {
                entity.ToTable("CurrenciesPelecardCode");
            });

            modelBuilder.Entity<Currency>(entity =>
            {
                entity.Property(e => e.CurrencyName).HasMaxLength(50);

                entity.Property(e => e.Symbol).HasMaxLength(10);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<DealAvailabilty>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("DealAvailabilty");

                entity.HasIndex(e => e.DealId, "IDX_DealAvailabilty_DealId");

                entity.HasIndex(e => e.InsertDate, "IDX_DealAvailabilty_InsertDate");

                entity.Property(e => e.InsertDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OrderId).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<DealAvalabilty>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("DealAvalabilty");

                entity.Property(e => e.InsertDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RoomsNumber).HasColumnName("RoomsNUmber");
            });

            modelBuilder.Entity<DealHostBaseAndPriceCode>(entity =>
            {
                entity.HasKey(e => e.DealId)
                    .HasName("PK__dealHost__1BC5A8715AB9788F");

                entity.ToTable("dealHostBaseAndPriceCode");

                entity.Property(e => e.DealId)
                    .ValueGeneratedNever()
                    .HasColumnName("dealId");

                entity.Property(e => e.HostBaseId)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("hostBaseId");

                entity.Property(e => e.PriceCodeId).HasColumnName("priceCodeId");

                entity.Property(e => e.PriceType).HasColumnName("priceType");

                entity.HasOne(d => d.PriceCode)
                    .WithMany(p => p.DealHostBaseAndPriceCodes)
                    .HasForeignKey(d => d.PriceCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__dealHostB__price__5CA1C101");
            });

            modelBuilder.Entity<DivurUser>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("divurUsers");

                entity.Property(e => e.Email)
                    .HasMaxLength(20)
                    .HasColumnName("email")
                    .IsFixedLength(true);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(15)
                    .HasColumnName("firstName")
                    .IsFixedLength(true);

                entity.Property(e => e.LastName)
                    .HasMaxLength(15)
                    .HasColumnName("lastName")
                    .IsFixedLength(true);

                entity.Property(e => e.RegisterDate)
                    .HasMaxLength(20)
                    .HasColumnName("registerDate")
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<Favorite>(entity =>
            {
                entity.ToTable("favorites");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.HotelId).HasColumnName("hotelId");

                entity.Property(e => e.UserId).HasColumnName("userId");
            });

            modelBuilder.Entity<G4ToGocAndSkyGoc>(entity =>
            {
                entity.ToTable("g4ToGocAndSkyGoc");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.G4Id)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("g4Id");

                entity.Property(e => e.GocId)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("gocId");

                entity.Property(e => e.ParentPriceCode)
                    .HasMaxLength(255)
                    .HasColumnName("parentPriceCode")
                    .IsFixedLength(true);

                entity.Property(e => e.SkyGocId)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("skyGocId");
            });

            modelBuilder.Entity<HostBase>(entity =>
            {
                entity.ToTable("hostBase");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AdultPrice).HasColumnName("adultPrice");

                entity.Property(e => e.ChildPrice).HasColumnName("childPrice");

                entity.Property(e => e.HostingBase)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("hostingBase");

                entity.Property(e => e.HotelId).HasColumnName("hotelId");

                entity.Property(e => e.IsActive).HasColumnName("isActive");

                entity.Property(e => e.Prio).HasColumnName("prio");

                entity.Property(e => e.SpecialName)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("specialName")
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<HotelAvailableHostBase>(entity =>
            {
                entity.ToTable("hotelAvailableHostBase");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.HostBaseId)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("hostBaseId");

                entity.Property(e => e.HotelFattalId).HasColumnName("hotelFattalId");

                entity.Property(e => e.IsActive).HasColumnName("isActive");
            });

            modelBuilder.Entity<HotelCompositionOptima>(entity =>
            {
                entity.ToTable("hotelCompositionOptima");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Hotelid)
                    .HasMaxLength(50)
                    .HasColumnName("hotelid");

                entity.Property(e => e.RoomCompositionName).HasColumnName("roomCompositionName");

                entity.Property(e => e.RoomId)
                    .HasMaxLength(50)
                    .HasColumnName("roomId");

                entity.Property(e => e.RoomName).HasColumnName("roomName");
            });

            modelBuilder.Entity<HotelPriceCodeAndPercentage>(entity =>
            {
                entity.ToTable("hotelPriceCodeAndPercentage");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ParentPriceCode)
                    .HasMaxLength(255)
                    .HasColumnName("parentPriceCode")
                    .IsFixedLength(true);

                entity.Property(e => e.Percentage).HasColumnName("percentage");

                entity.Property(e => e.PriceCode)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("priceCode");

                entity.Property(e => e.PriceType).HasColumnName("priceType");
            });

            modelBuilder.Entity<HotelRoomFeature>(entity =>
            {
                entity.Property(e => e.IsBaby).HasColumnName("isBaby");

                entity.Property(e => e.Name).HasMaxLength(150);

                entity.Property(e => e.RoomId)
                    .HasColumnName("roomId")
                    .HasDefaultValueSql("('')");

                entity.HasOne(d => d.Hotel)
                    .WithMany(p => p.HotelRoomFeatures)
                    .HasForeignKey(d => d.HotelId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_HotelRoomFeatures_cms");
            });

            modelBuilder.Entity<Log>(entity =>
            {
                entity.Property(e => e.AreaOfError)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ErrorDesc)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.InsertDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PageOfError)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Module>(entity =>
            {
                entity.Property(e => e.IsModuleOn).HasColumnName("isModuleOn");

                entity.Property(e => e.ModuleName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<News>(entity =>
            {
                entity.ToTable("news");

                entity.HasIndex(e => e.Show, "IDX_news_show");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CatId)
                    .HasMaxLength(50)
                    .HasColumnName("catId");

                entity.Property(e => e.Clicks).HasColumnName("clicks");

                entity.Property(e => e.Color)
                    .HasMaxLength(50)
                    .HasColumnName("color");

                entity.Property(e => e.FatherId)
                    .HasColumnName("fatherId")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.FiltersAttribution).HasColumnName("filtersAttribution");

                entity.Property(e => e.Idate)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("idate");

                entity.Property(e => e.IsFilters)
                    .HasColumnName("isFilters")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Language)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("language")
                    .IsFixedLength(true);

                entity.Property(e => e.Media1Path)
                    .HasMaxLength(500)
                    .HasColumnName("media1Path");

                entity.Property(e => e.Media2Path)
                    .HasMaxLength(500)
                    .HasColumnName("media2Path");

                entity.Property(e => e.Media3Path)
                    .IsRequired()
                    .HasColumnName("media3Path")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Media4Path)
                    .IsRequired()
                    .HasColumnName("media4Path")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Media5Path)
                    .IsRequired()
                    .HasColumnName("media5Path")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.MultipleSelection).HasColumnName("multipleSelection");

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .HasColumnName("name");

                entity.Property(e => e.Name2)
                    .HasColumnType("ntext")
                    .HasColumnName("name2");

                entity.Property(e => e.Name3)
                    .HasMaxLength(255)
                    .HasColumnName("name3");

                entity.Property(e => e.Prio).HasColumnName("prio");

                entity.Property(e => e.Show).HasColumnName("show");

                entity.Property(e => e.Showdate).HasColumnName("showdate");

                entity.Property(e => e.Target)
                    .HasMaxLength(16)
                    .IsUnicode(false)
                    .HasColumnName("target");

                entity.Property(e => e.Url)
                    .HasMaxLength(255)
                    .HasColumnName("url");
            });

            modelBuilder.Entity<OptimaG4hotelsPriceCodeAndId>(entity =>
            {
                entity.HasKey(e => e.HotelFattalId)
                    .HasName("PK__optimaG4__FEEBAEF54E53A1AA");

                entity.ToTable("optimaG4HotelsPriceCodeAndId");

                entity.Property(e => e.HotelFattalId)
                    .ValueGeneratedNever()
                    .HasColumnName("hotelFattalId");

                entity.Property(e => e.HotelG4id).HasColumnName("hotelG4Id");

                entity.Property(e => e.PriceCode)
                    .HasMaxLength(255)
                    .HasColumnName("priceCode");
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.HasIndex(e => new { e.HotelReservation, e.IsSentToOptima }, "IDX_Orders_hotel_reservation_isSentToOptima");

                entity.HasIndex(e => new { e.IsOrderCompleted, e.OrderStatusId, e.SsnOrder }, "IDX_orders_isOrderCompleted_ssnOrder_orderStatusId");

                entity.HasIndex(e => e.OrderDate, "IDX_orders_orderdate");

                entity.HasIndex(e => new { e.IsOrderCompleted, e.DealId, e.OrderStatusId }, "IX_Orders_isOrderCompleted_DealId_orderStatusId");

                entity.Property(e => e.Address).HasColumnName("address");

                entity.Property(e => e.Birthday)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("birthday");

                entity.Property(e => e.Cellular).HasMaxLength(50);

                entity.Property(e => e.City)
                    .HasMaxLength(50)
                    .HasColumnName("city");

                entity.Property(e => e.Comments).HasColumnName("comments");

                entity.Property(e => e.CommetsClient).HasColumnName("commetsClient");

                entity.Property(e => e.CreditCardIssur)
                    .HasMaxLength(10)
                    .HasColumnName("creditCardIssur")
                    .IsFixedLength(true);

                entity.Property(e => e.CreditCardType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("creditCardType")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Digit)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("digit")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Email)
                    .HasMaxLength(250)
                    .HasColumnName("email");

                entity.Property(e => e.Expire)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("expire")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.FinalComments)
                    .IsRequired()
                    .HasColumnName("final_comments")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.FinalLockedPrice).HasColumnName("finalLockedPrice");

                entity.Property(e => e.FinalPriceShowedNis).HasColumnName("FinalPriceShowedNIS");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(150)
                    .HasColumnName("firstName");

                entity.Property(e => e.HotelReservation)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("hotel_reservation")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.IsCommit).HasColumnName("isCommit");

                entity.Property(e => e.IsJoinSkyClub).HasColumnName("isJoinSkyClub");

                entity.Property(e => e.IsMailSent)
                    .HasColumnName("isMailSent")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IsMobile).HasColumnName("isMobile");

                entity.Property(e => e.IsOrderCompleted).HasColumnName("isOrderCompleted");

                entity.Property(e => e.IsPelecardSuccess).HasColumnName("isPelecardSuccess");

                entity.Property(e => e.IsRegisterNewsLetter).HasColumnName("isRegisterNewsLetter");

                entity.Property(e => e.IsRenewSkyClub).HasColumnName("isRenewSkyClub");

                entity.Property(e => e.IsSentToOptima).HasColumnName("isSentToOptima");

                entity.Property(e => e.IsSkyClubJoin).HasColumnName("isSkyClubJoin");

                entity.Property(e => e.IsSkyClubWasNotActvie)
                    .HasColumnName("isSkyClubWasNotActvie")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LastName)
                    .HasMaxLength(150)
                    .HasColumnName("lastName");

                entity.Property(e => e.ModeratorComments).HasColumnName("moderatorComments");

                entity.Property(e => e.OrderDate)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("orderDate");

                entity.Property(e => e.OrderMethod).HasMaxLength(50);

                entity.Property(e => e.OrderStatusId).HasColumnName("orderStatusId");

                entity.Property(e => e.Payments).HasColumnName("payments");

                entity.Property(e => e.PaypalautoExpire)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("paypalautoExpire");

                entity.Property(e => e.PelecardXmlIn)
                    .IsRequired()
                    .HasColumnName("pelecardXmlIn")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .HasColumnName("phone");

                entity.Property(e => e.Platforms)
                    .IsRequired()
                    .HasMaxLength(250)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.RedirectFrom)
                    .IsRequired()
                    .HasMaxLength(250)
                    .HasColumnName("redirectFrom")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.SessionId)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("sessionId")
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.SkyClubRooms).HasColumnName("skyClubRooms");

                entity.Property(e => e.SkyClubSsn)
                    .IsRequired()
                    .HasMaxLength(250)
                    .HasColumnName("skyClubSsn")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Ssn)
                    .HasMaxLength(50)
                    .HasColumnName("SSN");

                entity.Property(e => e.SsnOrder)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("ssnOrder")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Stamp)
                    .IsRequired()
                    .HasColumnName("stamp")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.SystemText).HasMaxLength(250);

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("updateTime");

                entity.Property(e => e.UserId).HasColumnName("userId");

                entity.Property(e => e.Utmsource)
                    .IsRequired()
                    .HasColumnName("UTMSource")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Zip).HasColumnName("zip");
            });

            modelBuilder.Entity<OrdersSpecialRequest>(entity =>
            {
                entity.ToTable("ordersSpecialRequests");

                entity.HasIndex(e => e.OrderId, "IDX_ordersSpecialRequests_orderId");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.OrderId).HasColumnName("orderId");

                entity.Property(e => e.RequestId).HasColumnName("requestId");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.OrdersSpecialRequests)
                    .HasForeignKey(d => d.OrderId)
                    .HasConstraintName("FK__ordersSpe__order__4A8310C6");

                entity.HasOne(d => d.Request)
                    .WithMany(p => p.OrdersSpecialRequests)
                    .HasForeignKey(d => d.RequestId)
                    .HasConstraintName("FK__ordersSpe__reque__4B7734FF");
            });

            modelBuilder.Entity<ParametersTbl>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ParametersTbl");

                entity.Property(e => e.PaymentServiceSemaphore).HasColumnName("paymentServiceSemaphore");
            });

            modelBuilder.Entity<PaymentServiceLog>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.InsertDate)
                    .HasColumnType("datetime")
                    .HasColumnName("insertDate")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LogData)
                    .HasMaxLength(4000)
                    .IsUnicode(false)
                    .HasColumnName("logData");

                entity.Property(e => e.OrderId).HasColumnName("orderId");
            });

            modelBuilder.Entity<PaypalHotel>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.ApiPass)
                    .HasMaxLength(200)
                    .HasColumnName("api_pass");

                entity.Property(e => e.ApiSignature).HasColumnName("api_signature");

                entity.Property(e => e.ApiUser)
                    .HasMaxLength(200)
                    .HasColumnName("api_user");

                entity.Property(e => e.HotelId).HasColumnName("hotelId");
            });

            modelBuilder.Entity<PelecardHotel>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.HotelId).HasColumnName("hotelId");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(e => e.TerminalNumber)
                    .IsRequired()
                    .HasColumnName("terminalNumber");
            });

            modelBuilder.Entity<PelecardSucceededOrder>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ConfirmationKey).HasColumnName("confirmationKey");

                entity.Property(e => e.Date)
                    .HasColumnType("datetime")
                    .HasColumnName("date");

                entity.Property(e => e.DebitApproveNumber).HasColumnName("debitApproveNumber");

                entity.Property(e => e.Json).HasColumnName("json");

                entity.Property(e => e.OrderId).HasColumnName("orderId");

                entity.Property(e => e.Token).HasColumnName("token");

                entity.Property(e => e.TransactionId).HasColumnName("transactionId");

                entity.Property(e => e.VoucherId).HasColumnName("voucherId");
            });

            modelBuilder.Entity<PlazmaCounter>(entity =>
            {
                entity.ToTable("PlazmaCounter");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Date)
                    .HasColumnType("datetime")
                    .HasColumnName("date");

                entity.Property(e => e.SavingCounter).HasColumnName("savingCounter");
            });

            modelBuilder.Entity<Redirect>(entity =>
            {
                entity.ToTable("Redirect");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.NewPath).HasColumnName("newPath");

                entity.Property(e => e.OldPath).HasColumnName("oldPath");
            });

            modelBuilder.Entity<Room>(entity =>
            {
                entity.ToTable("rooms");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BedTypes)
                    .HasMaxLength(200)
                    .HasColumnName("bedTypes")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");

                entity.Property(e => e.HotelId).HasColumnName("hotelId");

                entity.Property(e => e.IsActive).HasColumnName("isActive");

                entity.Property(e => e.IsWithDealDiscount).HasColumnName("isWithDealDiscount");

                entity.Property(e => e.MaxCapacity)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("maxCapacity")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Price).HasColumnName("price");

                entity.Property(e => e.Prio).HasColumnName("prio");

                entity.Property(e => e.RoomDevices)
                    .IsRequired()
                    .HasMaxLength(150)
                    .HasColumnName("roomDevices");

                entity.Property(e => e.RoomIdOptima)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("roomIdOptima")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.RoomImg)
                    .IsRequired()
                    .HasColumnName("roomImg");

                entity.Property(e => e.RoomName)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("roomName");

                entity.Property(e => e.RoomNameOptima)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("roomNameOptima")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.RoomSize)
                    .HasColumnName("roomSize")
                    .HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<SkyClubMember>(entity =>
            {
                entity.Property(e => e.Email)
                    .HasMaxLength(250)
                    .HasColumnName("email");

                entity.Property(e => e.Password)
                    .HasMaxLength(250)
                    .HasColumnName("password");

                entity.Property(e => e.Ssn)
                    .HasMaxLength(20)
                    .HasColumnName("SSN");
            });

            modelBuilder.Entity<SkyStatus>(entity =>
            {
                entity.ToTable("skyStatuses");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.OrderDate)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("orderDate");

                entity.Property(e => e.OrderId).HasColumnName("orderId");

                entity.Property(e => e.PelecardComments)
                    .IsUnicode(false)
                    .HasColumnName("pelecardComments");

                entity.Property(e => e.SkyPrice).HasColumnName("skyPrice");

                entity.Property(e => e.SkyStatus1).HasColumnName("skyStatus");

                entity.Property(e => e.SkyType)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("skyType");

                entity.Property(e => e.UpdateDate)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("updateDate");
            });

            modelBuilder.Entity<SpecialRequest>(entity =>
            {
                entity.ToTable("specialRequests");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasColumnName("isActive")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.IsActiveForOneRoom).HasColumnName("isActiveForOneRoom");

                entity.Property(e => e.RequestName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("requestName");
            });

            modelBuilder.Entity<TempCm>(entity =>
            {
                entity.ToTable("tempCms");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ActuallyDealEndTime).HasColumnType("smalldatetime");

                entity.Property(e => e.BannerId1).HasColumnName("bannerId1");

                entity.Property(e => e.BannerId2).HasColumnName("bannerId2");

                entity.Property(e => e.BannerId3).HasColumnName("bannerId3");

                entity.Property(e => e.BannerId4).HasColumnName("bannerId4");

                entity.Property(e => e.CatalogNumber).HasMaxLength(50);

                entity.Property(e => e.CmsLinkId1).HasColumnName("cmsLinkId1");

                entity.Property(e => e.CmsLinkId2).HasColumnName("cmsLinkId2");

                entity.Property(e => e.CmsViews).HasColumnName("cmsViews");

                entity.Property(e => e.DealArrivalDate).HasColumnType("smalldatetime");

                entity.Property(e => e.DealDefaultRoom).HasColumnName("dealDefaultRoom");

                entity.Property(e => e.DealDeptDate).HasColumnType("smalldatetime");

                entity.Property(e => e.DealEndDate).HasColumnType("smalldatetime");

                entity.Property(e => e.DealHb).HasColumnName("dealHB");

                entity.Property(e => e.DealRoomType).HasMaxLength(255);

                entity.Property(e => e.DealShortText).HasMaxLength(250);

                entity.Property(e => e.DealStartDate).HasColumnType("smalldatetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .HasColumnName("description");

                entity.Property(e => e.Descriptionfordeal).HasColumnName("descriptionfordeal");

                entity.Property(e => e.Directions)
                    .IsRequired()
                    .HasColumnName("directions");

                entity.Property(e => e.FatherId).HasColumnName("fatherId");

                entity.Property(e => e.Furl).HasColumnName("furl");

                entity.Property(e => e.HotelAddress).HasMaxLength(100);

                entity.Property(e => e.HotelArea)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("hotelArea");

                entity.Property(e => e.HotelBenefits)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("hotelBenefits");

                entity.Property(e => e.HotelEmail).HasMaxLength(250);

                entity.Property(e => e.HotelFax)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.HotelPhone).HasMaxLength(50);

                entity.Property(e => e.HotelTerminalId)
                    .HasMaxLength(10)
                    .IsFixedLength(true);

                entity.Property(e => e.HotelWelcomeAppLink).HasMaxLength(1000);

                entity.Property(e => e.IsBarPrint).HasColumnName("isBarPrint");

                entity.Property(e => e.IsContactUs).HasColumnName("isContactUs");

                entity.Property(e => e.IsDisplayedOnNavBar).HasColumnName("isDisplayedOnNavBar");

                entity.Property(e => e.IsGallery).HasColumnName("isGallery");

                entity.Property(e => e.IsHoliday).HasColumnName("isHoliday");

                entity.Property(e => e.IsInSiteMap).HasColumnName("isInSiteMap");

                entity.Property(e => e.IsLeftNavBar).HasColumnName("isLeftNavBar");

                entity.Property(e => e.IsNightShow).HasColumnName("isNightShow");

                entity.Property(e => e.IsPaused).HasColumnName("isPaused");

                entity.Property(e => e.IsRelNofollow).HasColumnName("isRelNofollow");

                entity.Property(e => e.IsRightNavBar).HasColumnName("isRightNavBar");

                entity.Property(e => e.IsSaturday).HasColumnName("isSaturday");

                entity.Property(e => e.IsSecure).HasColumnName("isSecure");

                entity.Property(e => e.IsShop).HasColumnName("isShop");

                entity.Property(e => e.IsShowMoreItemsAfter).HasColumnName("isShowMoreItemsAfter");

                entity.Property(e => e.IsSpecialOccasion).HasColumnName("isSpecialOccasion");

                entity.Property(e => e.IsTalkBack).HasColumnName("isTalkBack");

                entity.Property(e => e.Keywords)
                    .HasMaxLength(500)
                    .HasColumnName("keywords");

                entity.Property(e => e.Language)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("language")
                    .IsFixedLength(true);

                entity.Property(e => e.LinkTo)
                    .HasMaxLength(250)
                    .HasColumnName("linkTo");

                entity.Property(e => e.Map).HasColumnName("map");

                entity.Property(e => e.MapImg)
                    .IsRequired()
                    .HasColumnName("mapImg");

                entity.Property(e => e.MapLogo)
                    .HasMaxLength(100)
                    .HasColumnName("mapLogo");

                entity.Property(e => e.Media1Path)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("media1Path");

                entity.Property(e => e.Media1PathAlt)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("media1PathAlt");

                entity.Property(e => e.Media2Path)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("media2Path");

                entity.Property(e => e.Media2PathAlt)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("media2PathAlt");

                entity.Property(e => e.Media3Path)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("media3Path");

                entity.Property(e => e.Media3PathAlt)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("media3PathAlt");

                entity.Property(e => e.Media4Path)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("media4Path");

                entity.Property(e => e.Media4PathAlt)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("media4PathAlt");

                entity.Property(e => e.Media5Path)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("media5Path");

                entity.Property(e => e.Media5PathAlt)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("media5PathAlt");

                entity.Property(e => e.Media6Path)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("media6Path");

                entity.Property(e => e.Media6PathAlt)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("media6PathAlt");

                entity.Property(e => e.Media7Path)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("media7Path");

                entity.Property(e => e.Media7PathAlt)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("media7PathAlt");

                entity.Property(e => e.MiscText1)
                    .HasMaxLength(150)
                    .HasColumnName("miscText1");

                entity.Property(e => e.MiscText2)
                    .HasMaxLength(150)
                    .HasColumnName("miscText2");

                entity.Property(e => e.MiscText3)
                    .HasMaxLength(150)
                    .HasColumnName("miscText3");

                entity.Property(e => e.MiscText4)
                    .HasMaxLength(150)
                    .HasColumnName("miscText4");

                entity.Property(e => e.MoreBoxesStyle).HasColumnName("moreBoxesStyle");

                entity.Property(e => e.Motag)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("motag");

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .HasColumnName("name");

                entity.Property(e => e.Name2)
                    .HasMaxLength(500)
                    .HasColumnName("name2");

                entity.Property(e => e.NameBar)
                    .HasMaxLength(255)
                    .HasColumnName("nameBar");

                entity.Property(e => e.NameUrl)
                    .HasMaxLength(255)
                    .HasColumnName("nameUrl");

                entity.Property(e => e.NewsLinkId1).HasColumnName("newsLinkId1");

                entity.Property(e => e.OldId).HasColumnName("oldId");

                entity.Property(e => e.OldImageKey)
                    .HasMaxLength(250)
                    .HasColumnName("oldImageKey");

                entity.Property(e => e.Prio).HasColumnName("prio");

                entity.Property(e => e.Robots)
                    .HasMaxLength(55)
                    .IsUnicode(false)
                    .HasColumnName("robots");

                entity.Property(e => e.SDate)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("sDate");

                entity.Property(e => e.SapakId).HasColumnName("sapakId");

                entity.Property(e => e.ShopId).HasColumnName("shopId");

                entity.Property(e => e.ShopPrice).HasColumnName("shopPrice");

                entity.Property(e => e.Show).HasColumnName("show");

                entity.Property(e => e.SpecialOc)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("specialOc");

                entity.Property(e => e.Title)
                    .HasMaxLength(255)
                    .HasColumnName("title");

                entity.Property(e => e.Titlefordeal).HasColumnName("titlefordeal");

                entity.Property(e => e.Txt).HasColumnName("txt");

                entity.Property(e => e.Txt2).HasColumnName("txt2");

                entity.Property(e => e.Txt3).HasColumnName("txt3");

                entity.Property(e => e.TypeId)
                    .HasMaxLength(50)
                    .HasColumnName("typeId");

                entity.Property(e => e.Url)
                    .HasMaxLength(255)
                    .HasColumnName("url");

                entity.Property(e => e.VicType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("vicType");

                entity.Property(e => e.ViewsCount).HasColumnName("viewsCount");

                entity.Property(e => e.WebsiteStyle).HasColumnName("websiteStyle");

                entity.Property(e => e.WriterId).HasColumnName("writerId");
            });

            modelBuilder.Entity<UniqIpOrder>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("uniqIpOrder");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .HasColumnName("email");

                entity.Property(e => e.Firstname)
                    .HasMaxLength(50)
                    .HasColumnName("firstname");

                entity.Property(e => e.LastName)
                    .HasMaxLength(50)
                    .HasColumnName("lastName");

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .HasColumnName("phone");

                entity.Property(e => e.UniqIp).HasColumnName("uniqIp");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Address)
                    .HasMaxLength(200)
                    .HasColumnName("address");

                entity.Property(e => e.AnniversaryDate)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("anniversaryDate");

                entity.Property(e => e.BirthDate)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("birthDate");

                entity.Property(e => e.City)
                    .HasMaxLength(50)
                    .HasColumnName("city");

                entity.Property(e => e.Email)
                    .HasMaxLength(250)
                    .HasColumnName("email");

                entity.Property(e => e.EmailForDivur)
                    .IsRequired()
                    .HasMaxLength(250)
                    .HasColumnName("emailForDivur")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.FavorBrands)
                    .IsRequired()
                    .HasMaxLength(250)
                    .HasColumnName("favorBrands")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.FavorSeason)
                    .IsRequired()
                    .HasMaxLength(250)
                    .HasColumnName("favorSeason")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.FirstName).HasColumnName("firstName");

                entity.Property(e => e.Gender).HasColumnName("gender");

                entity.Property(e => e.Hobbies)
                    .IsRequired()
                    .HasMaxLength(250)
                    .HasColumnName("hobbies")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.IsDivur).HasColumnName("isDivur");

                entity.Property(e => e.IsMobile).HasColumnName("isMobile");

                entity.Property(e => e.IsVisit)
                    .HasColumnName("isVisit")
                    .HasDefaultValueSql("((2))");

                entity.Property(e => e.LastName).HasColumnName("lastName");

                entity.Property(e => e.LoginDate)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("loginDate");

                entity.Property(e => e.MateBirthDay)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("mateBirthDay")
                    .HasDefaultValueSql("('01/01/1910')");

                entity.Property(e => e.MateFullName)
                    .IsRequired()
                    .HasColumnName("mateFullName")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.MateGender)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("mateGender")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.MateSsn)
                    .IsRequired()
                    .HasMaxLength(250)
                    .HasColumnName("mateSsn")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Mobile)
                    .HasMaxLength(50)
                    .HasColumnName("mobile");

                entity.Property(e => e.MonthMulti)
                    .HasMaxLength(250)
                    .HasColumnName("monthMulti");

                entity.Property(e => e.Password)
                    .HasMaxLength(250)
                    .HasColumnName("password");

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .HasColumnName("phone");

                entity.Property(e => e.RegisterDate)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("registerDate");

                entity.Property(e => e.Rule1).HasColumnName("rule1");

                entity.Property(e => e.Show).HasColumnName("show");

                entity.Property(e => e.Ssn)
                    .IsRequired()
                    .HasMaxLength(250)
                    .HasColumnName("ssn")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.UtmSource)
                    .IsRequired()
                    .HasColumnName("utmSource")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.VicType)
                    .IsRequired()
                    .HasMaxLength(250)
                    .HasColumnName("vicType")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Zip)
                    .HasMaxLength(10)
                    .HasColumnName("zip");

                entity.Property(e => e.ZoneMulti)
                    .HasMaxLength(250)
                    .HasColumnName("zoneMulti");
            });

            modelBuilder.Entity<UserMessage>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Grade).HasColumnName("grade");

                entity.Property(e => e.Idate)
                    .HasColumnType("date")
                    .HasColumnName("idate");

                entity.Property(e => e.IsGrade).HasColumnName("isGrade");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("name");

                entity.Property(e => e.TestId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("testId");

                entity.Property(e => e.Txt)
                    .IsRequired()
                    .HasColumnName("txt");

                entity.Property(e => e.UserId).HasColumnName("userId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserMessages)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_UserMessages_Users");
            });

            modelBuilder.Entity<UsersChild>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ChildBirthDay)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("childBirthDay");

                entity.Property(e => e.ChildFullName).HasColumnName("childFullName");

                entity.Property(e => e.UserId).HasColumnName("userId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UsersChildren)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_UsersChilds_Users");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
