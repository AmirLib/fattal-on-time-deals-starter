﻿using FattalOnTimeDealsStarter.Domain.Interfaces;
using FattalOnTimeDealsStarter.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FattalOnTimeDealsStarter.Infra.Data.Repositories
{
    public class AutomaticDealsDataRepository: Repository, IAutomaticDealsDataRepository
    {
        public AutomaticDealsDataRepository(FattalNewContext ctx) : base(ctx) { }

        public AutomaticDealsData Get(int hotelId, DateTime arrivalDate, DateTime deptDate)
        {
            return _ctx.AutomaticDealsData
                .FirstOrDefault(data =>
                    data.HotelId == hotelId
                    && data.ArrivalDate == arrivalDate
                    && data.DeptDate == deptDate);
        }

        public List<AutomaticDealsData> GetAllByNotMailSent()
        {
            return GetAllByMailSentValue(false);
        }

        public void InsertNewInformation(
            int hotelId,
            DateTime arrivalDate,
            DateTime deptDate,
            string priceCode)
        {
            if (IsExists(hotelId, arrivalDate, deptDate)) return;

            _ctx.AutomaticDealsData.Add(new AutomaticDealsData
            {
                HotelId = hotelId,
                ArrivalDate = arrivalDate,
                DeptDate = deptDate,
                PriceCode = priceCode,
            });

            _ctx.SaveChanges();
        }

        public bool IsExists(int hotelId, DateTime arrivalDate, DateTime deptDate)
        {
            var data = _ctx.AutomaticDealsData
                .FirstOrDefault(data => 
                    data.HotelId == hotelId
                    && data.ArrivalDate == arrivalDate
                    && data.DeptDate == deptDate);

            return data != null;
        }

        #region Private Methods
        private List<AutomaticDealsData> GetAllByMailSentValue(bool value)
        {
            return _ctx.AutomaticDealsData
                .Where(data => data.MailSent == value)
                .ToList();
        } 
        #endregion
    }
}
