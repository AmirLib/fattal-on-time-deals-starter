﻿using FattalOnTimeDealsStarter.Domain.Interfaces;
using FattalOnTimeDealsStarter.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace FattalOnTimeDealsStarter.Infra.Data.Repositories
{
    public class DealHostBaseAndPriceCodeRepository : Repository, IDealHostBaseAndPriceCodeRepository
    {
        public DealHostBaseAndPriceCodeRepository(FattalNewContext ctx) : base(ctx) { }

        public List<IGrouping<int, DealHostBaseAndPriceCode>> GetDealsByPrecentageGroups(HashSet<int> ids)
        {
            var deals = _ctx.DealHostBaseAndPriceCodes
                .Where(deal => ids.Contains(deal.DealId))
                .Include(deal => deal.PriceCode)
                .AsEnumerable()
                .GroupBy(deal => deal.PriceCode.Percentage);

            return OrderPrecentageByPriceType(deals).ToList();
        }

        #region Private Methods
        private IOrderedEnumerable<IGrouping<int, DealHostBaseAndPriceCode>> OrderPrecentageByPriceType(IEnumerable<IGrouping<int, DealHostBaseAndPriceCode>> deals)
        {
            var isFixedPrice = deals.Any(group => group.Any(deal => deal.PriceType.HasValue && deal.PriceType.Value == 2));

            return isFixedPrice ?
                OrderByFixPriceType(deals) :
                OrderByPresentagePriceType(deals);
        }

        private IOrderedEnumerable<IGrouping<int, DealHostBaseAndPriceCode>> OrderByFixPriceType(IEnumerable<IGrouping<int, DealHostBaseAndPriceCode>> deals)
        {
            return deals.OrderBy(group => group.Key);
        }

        private IOrderedEnumerable<IGrouping<int, DealHostBaseAndPriceCode>> OrderByPresentagePriceType(IEnumerable<IGrouping<int, DealHostBaseAndPriceCode>> deals)
        {
            return deals.OrderByDescending(group => group.Key);
        }
        #endregion
    }
}
