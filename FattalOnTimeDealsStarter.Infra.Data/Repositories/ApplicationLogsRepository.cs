﻿using FattalOnTimeDealsStarter.Domain.Interfaces;
using FattalOnTimeDealsStarter.Domain.Models;

namespace FattalOnTimeDealsStarter.Infra.Data.Repositories
{
    public class ApplicationLogsRepository: Repository, IApplicationLogRepository
    {
        public ApplicationLogsRepository(FattalNewContext ctx): base(ctx) { }

        public void Add(ApplicationLog applicationLog)
        {
            _ctx.ApplicationLogs.Add(applicationLog);
            _ctx.SaveChanges();
        }
    }
}
