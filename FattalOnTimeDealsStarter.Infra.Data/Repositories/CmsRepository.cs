﻿using FattalOnTimeDealsStarter.Domain.Interfaces;
using FattalOnTimeDealsStarter.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FattalOnTimeDealsStarter.Infra.Data.Repositories
{
    public class CmsRepository : Repository, ICmsRepository
    {
        public CmsRepository(FattalNewContext ctx) : base(ctx) { }

        public List<Cm> GetAllActive()
        {
            return _ctx.Cms
                .Where(entity =>
                    entity.TypeId.Equals("Deals")
                    && (entity.DealEndDate >= DateTime.Now
                    || entity.ActuallyDealEndTime >= DateTime.Now))
                .ToList();
        }

        public Dictionary<int, Cm> GetAllActiveAsDictionary()
        {
            return _ctx.Cms
                .Where(entity =>
                    entity.TypeId.Equals("Deals")
                    && (entity.DealEndDate >= DateTime.Now
                    || entity.ActuallyDealEndTime >= DateTime.Now))
                .ToDictionary(entity => entity.Id);
        }

        public Cm GetNameBarById(int id)
        {
            return _ctx.Cms.FirstOrDefault(entity => entity.Id == id);
        }
    }
}
