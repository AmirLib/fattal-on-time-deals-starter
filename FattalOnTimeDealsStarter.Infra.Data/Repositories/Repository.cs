﻿using FattalOnTimeDealsStarter.Domain.Interfaces;

namespace FattalOnTimeDealsStarter.Infra.Data.Repositories
{
    public abstract class Repository : IRepository
    {
        public Repository(FattalNewContext ctx)
        {
            _ctx = ctx;
        }

        #region Fields
        public FattalNewContext _ctx;
        #endregion

        #region Private Methods
        public void SaveChanges()
        {
            _ctx.SaveChanges();
        }
        #endregion
    }
}
