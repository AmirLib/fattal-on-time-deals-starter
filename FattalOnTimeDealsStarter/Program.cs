﻿using FattalOnTimeDealsStarter.Application;
using FattalOnTimeDealsStarter.Application.Configurations;
using FattalOnTimeDealsStarter.Application.Interfaces;
using FattalOnTimeDealsStarter.Application.Services;
using FattalOnTimeDealsStarter.Domain.Interfaces;
using FattalOnTimeDealsStarter.Infra.Data;
using FattalOnTimeDealsStarter.Infra.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.IO;

namespace FattalOnTimeDealsStarter
{
    class Program
    {
        public static IConfigurationRoot configuration;

        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            var service = host.Services.GetRequiredService<DealsStarter>();

            service.Run();
        }

        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
                .AddJsonFile("appsettings.json", false)
                .Build();

            var connections = configuration
                .GetSection("ConnectionStrings")
                .Get<ConnectionsOptions>();

            return Host
                .CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddSingleton(configuration);
                    services.AddLogging(configure => configure.AddConsole());
                    services.AddDbContext<FattalNewContext>(
                        options =>
                        {
                            options
                                .UseSqlServer(connections.DbEntity)
                                .LogTo(Console.WriteLine, LogLevel.Debug);
                        });
                    services.AddScoped<IAutomaticDealsDataRepository, AutomaticDealsDataRepository>();
                    services.AddScoped<IAutomaticDealsDataService, AutomaticDealsDataService>();
                    services.AddScoped<ICmsRepository, CmsRepository>();
                    services.AddScoped<IDealHostBaseAndPriceCodeRepository, DealHostBaseAndPriceCodeRepository>();
                    services.AddScoped<ICmsService, CmsService>();
                    services.AddScoped<IEmailService, EmailService>();
                    services.AddScoped<IApplicationLogsService, ApplicationLogsService>();
                    services.AddScoped<IApplicationLogRepository, ApplicationLogsRepository>();
                    services.AddTransient<DealsStarter>();
                });
        }
    }
}
