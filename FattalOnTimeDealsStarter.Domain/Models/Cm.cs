﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class Cm
    {
        public Cm()
        {
            HotelRoomFeatures = new HashSet<HotelRoomFeature>();
        }

        public int Id { get; set; }
        public int? FatherId { get; set; }
        public string TypeId { get; set; }
        public short? WriterId { get; set; }
        public short? SapakId { get; set; }
        public string NameUrl { get; set; }
        public string NameBar { get; set; }
        public string Name { get; set; }
        public string Name2 { get; set; }
        public string Txt { get; set; }
        public string Txt2 { get; set; }
        public string Txt3 { get; set; }
        public string Url { get; set; }
        public bool? IsGallery { get; set; }
        public bool? IsTalkBack { get; set; }
        public bool? IsBarPrint { get; set; }
        public bool? IsShowMoreItemsAfter { get; set; }
        public bool? IsInSiteMap { get; set; }
        public bool? IsContactUs { get; set; }
        public bool? IsLeftNavBar { get; set; }
        public bool? IsRightNavBar { get; set; }
        public bool? IsSecure { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }
        public string Robots { get; set; }
        public bool? IsRelNofollow { get; set; }
        public short Prio { get; set; }
        public bool? Show { get; set; }
        public string Media1Path { get; set; }
        public string Media2Path { get; set; }
        public string Media3Path { get; set; }
        public string Media4Path { get; set; }
        public string Media5Path { get; set; }
        public string Media6Path { get; set; }
        public string Media7Path { get; set; }
        public string Language { get; set; }
        public bool? IsShop { get; set; }
        public double? ShopPrice { get; set; }
        public int? WebsiteStyle { get; set; }
        public int? MoreBoxesStyle { get; set; }
        public int? ShopId { get; set; }
        public int? ViewsCount { get; set; }
        public int? BannerId1 { get; set; }
        public int? BannerId2 { get; set; }
        public int? BannerId3 { get; set; }
        public int? BannerId4 { get; set; }
        public int? CmsViews { get; set; }
        public int? OldId { get; set; }
        public string CatalogNumber { get; set; }
        public string OldImageKey { get; set; }
        public int? NewsLinkId1 { get; set; }
        public string LinkTo { get; set; }
        public bool? IsDisplayedOnNavBar { get; set; }
        public DateTime? SDate { get; set; }
        public string MiscText1 { get; set; }
        public string MiscText2 { get; set; }
        public string MiscText3 { get; set; }
        public string MiscText4 { get; set; }
        public int? CmsLinkId1 { get; set; }
        public int? CmsLinkId2 { get; set; }
        public bool? IsNightShow { get; set; }
        public string HotelAddress { get; set; }
        public string HotelPhone { get; set; }
        public string HotelEmail { get; set; }
        public int? HotelZoneId { get; set; }
        public DateTime? DealArrivalDate { get; set; }
        public int? DealAvailableQuantity { get; set; }
        public int? DealStatusId { get; set; }
        public DateTime? DealDeptDate { get; set; }
        public int? DealDiscountPercent { get; set; }
        public DateTime? DealEndDate { get; set; }
        public int? DealMaxRooms { get; set; }
        public string DealRoomType { get; set; }
        public DateTime? DealStartDate { get; set; }
        public int? DealTotalQuantity { get; set; }
        public int? DealHotelId { get; set; }
        public double? DealPrice { get; set; }
        public string DealShortText { get; set; }
        public bool? DealIsDisplayedOnHomePage { get; set; }
        public string Map { get; set; }
        public string Furl { get; set; }
        public string Descriptionfordeal { get; set; }
        public string Titlefordeal { get; set; }
        public DateTime ActuallyDealEndTime { get; set; }
        public string MapImg { get; set; }
        public string Directions { get; set; }
        public bool IsSpecialOccasion { get; set; }
        public string Motag { get; set; }
        public string VicType { get; set; }
        public string HotelArea { get; set; }
        public string SpecialOc { get; set; }
        public string HotelBenefits { get; set; }
        public int DealHb { get; set; }
        public int DealDefaultRoom { get; set; }
        public bool IsSaturday { get; set; }
        public bool IsHoliday { get; set; }
        public string HotelFax { get; set; }
        public string Media1PathAlt { get; set; }
        public string Media2PathAlt { get; set; }
        public string Media3PathAlt { get; set; }
        public string Media4PathAlt { get; set; }
        public string Media5PathAlt { get; set; }
        public string Media6PathAlt { get; set; }
        public string Media7PathAlt { get; set; }
        public string HotelTerminalId { get; set; }
        public string MapLogo { get; set; }
        public bool? IsPaused { get; set; }
        public bool? HotelHasWelcomeApp { get; set; }
        public int? HotelCityId { get; set; }
        public string HotelWelcomeAppLink { get; set; }
        public bool? IsLocalCurrencyDeal { get; set; }
        public Guid? AutomaticDealsId { get; set; }

        public virtual ICollection<HotelRoomFeature> HotelRoomFeatures { get; set; }
    }
}
