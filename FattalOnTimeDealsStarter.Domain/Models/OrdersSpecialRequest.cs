﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class OrdersSpecialRequest
    {
        public int Id { get; set; }
        public int? OrderId { get; set; }
        public int? RequestId { get; set; }

        public virtual Order Order { get; set; }
        public virtual SpecialRequest Request { get; set; }
    }
}
