﻿using System;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class PaymentServiceLog
    {
        public DateTime? InsertDate { get; set; }
        public int OrderId { get; set; }
        public string LogData { get; set; }
        public double? Price { get; set; }
    }
}
