﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class User
    {
        public User()
        {
            UserMessages = new HashSet<UserMessage>();
            UsersChildren = new HashSet<UsersChild>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public bool? Gender { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool? Show { get; set; }
        public DateTime? BirthDate { get; set; }
        public DateTime? RegisterDate { get; set; }
        public DateTime? LoginDate { get; set; }
        public bool? IsDivur { get; set; }
        public DateTime? AnniversaryDate { get; set; }
        public int? CountryId { get; set; }
        public int? StateId { get; set; }
        public bool? Rule1 { get; set; }
        public string MonthMulti { get; set; }
        public string ZoneMulti { get; set; }
        public string UtmSource { get; set; }
        public bool IsMobile { get; set; }
        public string Ssn { get; set; }
        public string VicType { get; set; }
        public string Hobbies { get; set; }
        public string FavorSeason { get; set; }
        public string EmailForDivur { get; set; }
        public string MateFullName { get; set; }
        public string MateSsn { get; set; }
        public DateTime MateBirthDay { get; set; }
        public string FavorBrands { get; set; }
        public string MateGender { get; set; }
        public byte IsVisit { get; set; }

        public virtual ICollection<UserMessage> UserMessages { get; set; }
        public virtual ICollection<UsersChild> UsersChildren { get; set; }
    }
}
