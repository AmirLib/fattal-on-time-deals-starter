﻿using System;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class ApplicationLogsHelper
    {
        public int? Type { get; set; }
        public string Query { get; set; }
        public int? NumberOfErrorsToNotify { get; set; }
        public int? TimeIntervalBetweenCheckes { get; set; }
        public DateTime? LastExecution { get; set; }
        public DateTime? LastSent { get; set; }
        public string MessageSubject { get; set; }
        public string Message { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsIncludeData { get; set; }
        public int? ExecutionCount { get; set; }
        public int? NumberOfNotificationsBeforeStopNotify { get; set; }
        public int? TimeFromStopNotifyToReturnNotifyInMinutes { get; set; }
    }
}
