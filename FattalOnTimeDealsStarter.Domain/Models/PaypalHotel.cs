﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class PaypalHotel
    {
        public int? HotelId { get; set; }
        public string ApiUser { get; set; }
        public string ApiPass { get; set; }
        public string ApiSignature { get; set; }
    }
}
