﻿using System;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class PlazmaCounter
    {
        public DateTime Date { get; set; }
        public int SavingCounter { get; set; }
        public int Id { get; set; }
    }
}
