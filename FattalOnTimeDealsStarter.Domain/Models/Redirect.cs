﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class Redirect
    {
        public int Id { get; set; }
        public string OldPath { get; set; }
        public string NewPath { get; set; }
    }
}
