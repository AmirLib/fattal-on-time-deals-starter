﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class HotelCompositionOptima
    {
        public int Id { get; set; }
        public string Hotelid { get; set; }
        public string RoomCompositionName { get; set; }
        public string RoomId { get; set; }
        public string RoomName { get; set; }
    }
}
