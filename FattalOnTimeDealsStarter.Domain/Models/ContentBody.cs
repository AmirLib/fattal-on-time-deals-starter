﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class ContentBody
    {
        public int Id { get; set; }
        public int? ContentId { get; set; }
        public string Body { get; set; }
        public string ImageUrl { get; set; }
        public string ImageLink { get; set; }
        public string VideoUrl { get; set; }
    }
}
