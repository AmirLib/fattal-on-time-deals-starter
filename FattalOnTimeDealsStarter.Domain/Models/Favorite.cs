﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class Favorite
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int? HotelId { get; set; }
    }
}
