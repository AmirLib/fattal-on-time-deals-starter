﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class Coupon
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? MaxAmount { get; set; }
        public double? Price { get; set; }
        public bool? IsPercent { get; set; }
        public bool? Show { get; set; }
        public string Code { get; set; }
        public int? UsedAmount { get; set; }
    }
}
