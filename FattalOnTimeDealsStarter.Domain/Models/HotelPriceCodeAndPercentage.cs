﻿using System.Collections.Generic;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class HotelPriceCodeAndPercentage
    {
        public HotelPriceCodeAndPercentage()
        {
            DealHostBaseAndPriceCodes = new HashSet<DealHostBaseAndPriceCode>();
        }

        public int Id { get; set; }
        public string ParentPriceCode { get; set; }
        public string PriceCode { get; set; }
        public int Percentage { get; set; }
        public byte? PriceType { get; set; }

        public virtual ICollection<DealHostBaseAndPriceCode> DealHostBaseAndPriceCodes { get; set; }
    }
}
