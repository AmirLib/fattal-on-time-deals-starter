﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class Contact
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public bool? IsPhone { get; set; }
        public string Email { get; set; }
        public bool? IsEmail { get; set; }
    }
}
