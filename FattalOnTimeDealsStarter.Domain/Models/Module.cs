﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class Module
    {
        public int ModuleId { get; set; }
        public bool IsModuleOn { get; set; }
        public string ModuleName { get; set; }
    }
}
