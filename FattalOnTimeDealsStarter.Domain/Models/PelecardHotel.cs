﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class PelecardHotel
    {
        public int HotelId { get; set; }
        public int Id { get; set; }
        public string TerminalNumber { get; set; }
    }
}
