﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class Room
    {
        public int Id { get; set; }
        public string RoomName { get; set; }
        public int HotelId { get; set; }
        public string Description { get; set; }
        public string RoomDevices { get; set; }
        public string RoomImg { get; set; }
        public double Price { get; set; }
        public bool IsActive { get; set; }
        public int Prio { get; set; }
        public string RoomIdOptima { get; set; }
        public string RoomNameOptima { get; set; }
        public string MaxCapacity { get; set; }
        public string BedTypes { get; set; }
        public double? RoomSize { get; set; }
        public bool? IsWithDealDiscount { get; set; }
        public bool IsAccessible { get; set; }
    }
}
