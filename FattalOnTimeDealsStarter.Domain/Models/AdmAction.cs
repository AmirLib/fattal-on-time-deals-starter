﻿using System;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class AdmAction
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string ActionType { get; set; }
        public string ActionTable { get; set; }
        public int? ActionObjectId { get; set; }
        public DateTime? ActionDate { get; set; }
    }
}
