﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class ApplicationLogsContact
    {
        public int Id { get; set; }
        public int? LogType { get; set; }
        public int? ContactId { get; set; }
    }
}
