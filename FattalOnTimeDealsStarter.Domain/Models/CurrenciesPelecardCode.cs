﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class CurrenciesPelecardCode
    {
        public int Id { get; set; }
        public int? CurrencyId { get; set; }
        public int? PelecardCode { get; set; }
    }
}
