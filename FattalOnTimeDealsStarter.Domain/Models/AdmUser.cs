﻿using System;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class AdmUser
    {
        public int Id { get; set; }
        public short? StatusAuth { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Job { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public DateTime? Idate { get; set; }
        public DateTime? Udate { get; set; }
        public DateTime? LastLogindate { get; set; }
        public bool? Show { get; set; }
        public short? Prio { get; set; }
    }
}
