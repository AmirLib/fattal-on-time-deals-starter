﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class UniqIpOrder
    {
        public string UniqIp { get; set; }
        public string Firstname { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
