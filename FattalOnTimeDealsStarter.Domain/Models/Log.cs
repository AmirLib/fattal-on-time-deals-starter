﻿using System;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class Log
    {
        public int Id { get; set; }
        public int? OrderId { get; set; }
        public DateTime? InsertDate { get; set; }
        public string PageOfError { get; set; }
        public string AreaOfError { get; set; }
        public string ErrorDesc { get; set; }
    }
}
