﻿using System;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class CancelationOrder
    {
        public string FirstName { get; set; }
        public string Lastname { get; set; }
        public string IdCustomer { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int? OrderId { get; set; }
        public string HotelName { get; set; }
        public string Comments { get; set; }
        public DateTime? Date { get; set; }
    }
}
