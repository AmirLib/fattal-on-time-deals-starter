﻿using System;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class DealAvailabilty
    {
        public int? DealId { get; set; }
        public int? RoomsNumber { get; set; }
        public DateTime? InsertDate { get; set; }
        public int? OrderId { get; set; }
    }
}
