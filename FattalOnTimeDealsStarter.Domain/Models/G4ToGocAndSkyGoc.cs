﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class G4ToGocAndSkyGoc
    {
        public int Id { get; set; }
        public string ParentPriceCode { get; set; }
        public string G4Id { get; set; }
        public string GocId { get; set; }
        public string SkyGocId { get; set; }
    }
}
