﻿using System;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class PelecardSucceededOrder
    {
        public int Id { get; set; }
        public int? OrderId { get; set; }
        public DateTime? Date { get; set; }
        public string ConfirmationKey { get; set; }
        public string TransactionId { get; set; }
        public string Json { get; set; }
        public string Token { get; set; }
        public string DebitApproveNumber { get; set; }
        public string VoucherId { get; set; }
    }
}
