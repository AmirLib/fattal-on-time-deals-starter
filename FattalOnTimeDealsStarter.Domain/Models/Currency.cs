﻿using System;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class Currency
    {
        public int Id { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string CurrencyName { get; set; }
        public double? Value { get; set; }
        public string Symbol { get; set; }
        public bool? Active { get; set; }
    }
}
