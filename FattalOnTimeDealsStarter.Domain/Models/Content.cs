﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class Content
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public int SectionId { get; set; }
        public string Url { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string HeaderImageUrl { get; set; }
        public string SeoTitle { get; set; }
        public string SeoDescription { get; set; }
        public string SeoKeyWords { get; set; }
        public string SeoRobots { get; set; }
        public string SeoLinkType { get; set; }

        public virtual News Section { get; set; }
    }
}
