﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class DealHostBaseAndPriceCode
    {
        public int DealId { get; set; }
        public string HostBaseId { get; set; }
        public int PriceCodeId { get; set; }
        public int? PriceType { get; set; }

        public virtual HotelPriceCodeAndPercentage PriceCode { get; set; }
    }
}
