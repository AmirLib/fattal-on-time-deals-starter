﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class OptimaG4hotelsPriceCodeAndId
    {
        public int HotelFattalId { get; set; }
        public int HotelG4id { get; set; }
        public string PriceCode { get; set; }
    }
}
