﻿using System;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class UsersChild
    {
        public int? UserId { get; set; }
        public string ChildFullName { get; set; }
        public DateTime? ChildBirthDay { get; set; }
        public int Id { get; set; }

        public virtual User User { get; set; }
    }
}
