﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class CmsGallery
    {
        public int Id { get; set; }
        public int? CmsId { get; set; }
        public string Name { get; set; }
        public string ImgPath1 { get; set; }
        public string ImgPath2 { get; set; }
        public string ImgPath3 { get; set; }
        public string ImgPath4 { get; set; }
        public int? Prio { get; set; }
        public bool? Show { get; set; }
        public string Descr { get; set; }
        public bool? IsGallery { get; set; }
        public bool IsRoom { get; set; }
        public bool IsDiploma { get; set; }
        public bool IsVideo { get; set; }
        public bool? IsReason { get; set; }
        public bool? IsEntertainment { get; set; }
    }
}
