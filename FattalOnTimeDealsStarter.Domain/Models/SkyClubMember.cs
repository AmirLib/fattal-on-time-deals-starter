﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class SkyClubMember
    {
        public int Id { get; set; }
        public string Ssn { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
