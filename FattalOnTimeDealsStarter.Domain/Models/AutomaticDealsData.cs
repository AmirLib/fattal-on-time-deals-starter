﻿using System;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public class AutomaticDealsData
    {
        public int Id { get; set; }
        public int HotelId { get; set; }
        public bool MailSent { get; set; }
        public bool AllClosed { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime DeptDate { get; set; }
        public string PriceCode { get; set; }
    }
}
