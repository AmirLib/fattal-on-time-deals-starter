﻿using System;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class SkyStatus
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public DateTime? OrderDate { get; set; }
        public int? SkyStatus1 { get; set; }
        public string SkyType { get; set; }
        public double? SkyPrice { get; set; }
        public string PelecardComments { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
