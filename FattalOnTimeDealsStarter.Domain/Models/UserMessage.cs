﻿using System;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class UserMessage
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public DateTime Idate { get; set; }
        public string Txt { get; set; }
        public bool IsGrade { get; set; }
        public int Grade { get; set; }
        public string TestId { get; set; }

        public virtual User User { get; set; }
    }
}
