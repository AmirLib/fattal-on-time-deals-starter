﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class HotelAvailableHostBase
    {
        public int Id { get; set; }
        public int HotelFattalId { get; set; }
        public string HostBaseId { get; set; }
        public bool IsActive { get; set; }
    }
}
