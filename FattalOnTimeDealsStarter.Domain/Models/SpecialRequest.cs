﻿using System.Collections.Generic;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class SpecialRequest
    {
        public SpecialRequest()
        {
            OrdersSpecialRequests = new HashSet<OrdersSpecialRequest>();
        }

        public int Id { get; set; }
        public string RequestName { get; set; }
        public bool? IsActive { get; set; }
        public int? IsActiveForOneRoom { get; set; }

        public virtual ICollection<OrdersSpecialRequest> OrdersSpecialRequests { get; set; }
    }
}
