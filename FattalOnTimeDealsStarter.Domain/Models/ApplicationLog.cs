﻿using System;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class ApplicationLog
    {
        public int Id { get; set; }
        public int? LogLevel { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? Type { get; set; }
        public int? OrderId { get; set; }
        public string MethodName { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public string ServerIp { get; set; }
        public string UserDevice { get; set; }
        public string UserBrowser { get; set; }
    }
}
