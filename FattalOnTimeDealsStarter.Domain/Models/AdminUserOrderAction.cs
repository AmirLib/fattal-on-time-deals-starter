﻿using System;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class AdminUserOrderAction
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string ActionType { get; set; }
        public DateTime? ActionDate { get; set; }
        public int? OrderId { get; set; }
    }
}
