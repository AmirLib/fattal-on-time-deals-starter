﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class ParametersTbl
    {
        public int PaymentServiceSemaphore { get; set; }
    }
}
