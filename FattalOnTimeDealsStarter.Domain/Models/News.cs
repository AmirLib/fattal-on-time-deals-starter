﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class News
    {
        public News()
        {
            Contents = new HashSet<Content>();
        }

        public int Id { get; set; }
        public string CatId { get; set; }
        public string Name { get; set; }
        public string Name2 { get; set; }
        public string Url { get; set; }
        public string Target { get; set; }
        public DateTime? Idate { get; set; }
        public bool? Showdate { get; set; }
        public bool? Show { get; set; }
        public short? Prio { get; set; }
        public string Media1Path { get; set; }
        public string Media2Path { get; set; }
        public int? Clicks { get; set; }
        public string Language { get; set; }
        public string Color { get; set; }
        public string Name3 { get; set; }
        public string Media3Path { get; set; }
        public string Media4Path { get; set; }
        public string Media5Path { get; set; }
        public int? FatherId { get; set; }
        public bool? IsFilters { get; set; }
        public string MultipleSelection { get; set; }
        public string FiltersAttribution { get; set; }
        public int? CurrencyId { get; set; }

        public virtual ICollection<Content> Contents { get; set; }
    }
}
