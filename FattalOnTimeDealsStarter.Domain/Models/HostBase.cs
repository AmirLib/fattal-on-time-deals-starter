﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class HostBase
    {
        public int Id { get; set; }
        public int HotelId { get; set; }
        public string HostingBase { get; set; }
        public bool IsActive { get; set; }
        public int Prio { get; set; }
        public double AdultPrice { get; set; }
        public double ChildPrice { get; set; }
        public string SpecialName { get; set; }
    }
}
