﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class HotelRoomFeature
    {
        public int Id { get; set; }
        public int? HotelId { get; set; }
        public string Name { get; set; }
        public int? Price { get; set; }
        public int? PercentPrice { get; set; }
        public bool? IsActive { get; set; }
        public bool IsBaby { get; set; }
        public int RoomId { get; set; }

        public virtual Cm Hotel { get; set; }
    }
}
