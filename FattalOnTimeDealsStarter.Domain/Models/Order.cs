﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class Order
    {
        public Order()
        {
            OrdersSpecialRequests = new HashSet<OrdersSpecialRequest>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Comments { get; set; }
        public int? Payments { get; set; }
        public int? OrderStatusId { get; set; }
        public int? UserId { get; set; }
        public DateTime? OrderDate { get; set; }
        public bool? IsOrderCompleted { get; set; }
        public string Cellular { get; set; }
        public string OrderMethod { get; set; }
        public string ModeratorComments { get; set; }
        public double? FinalLockedPrice { get; set; }
        public string SystemText { get; set; }
        public string Token { get; set; }
        public int? DealId { get; set; }
        public string Ssn { get; set; }
        public string CreditCardIssur { get; set; }
        public string CommetsClient { get; set; }
        public string FinalComments { get; set; }
        public string HotelReservation { get; set; }
        public string CreditCardType { get; set; }
        public string Digit { get; set; }
        public string Expire { get; set; }
        public string Stamp { get; set; }
        public bool IsSentToOptima { get; set; }
        public string SkyClubSsn { get; set; }
        public string RedirectFrom { get; set; }
        public string Platforms { get; set; }
        public string Utmsource { get; set; }
        public bool IsMobile { get; set; }
        public string SsnOrder { get; set; }
        public DateTime? UpdateTime { get; set; }
        public DateTime? PaypalautoExpire { get; set; }
        public int SkyClubRooms { get; set; }
        public bool IsSkyClubJoin { get; set; }
        public bool IsJoinSkyClub { get; set; }
        public bool IsCommit { get; set; }
        public string PelecardXmlIn { get; set; }
        public bool IsRenewSkyClub { get; set; }
        public string SessionId { get; set; }
        public bool? IsMailSent { get; set; }
        public DateTime? Birthday { get; set; }
        public bool? IsRegisterNewsLetter { get; set; }
        public bool? IsPelecardSuccess { get; set; }
        public bool? IsSkyClubWasNotActvie { get; set; }
        public int? CurrencyId { get; set; }
        public double? CurrencyRate { get; set; }
        public double? FinalPriceShowedNis { get; set; }
        public bool IsFatalError { get; set; }

        public virtual ICollection<OrdersSpecialRequest> OrdersSpecialRequests { get; set; }
    }
}
