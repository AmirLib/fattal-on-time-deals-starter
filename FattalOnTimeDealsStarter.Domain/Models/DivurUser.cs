﻿#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class DivurUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string RegisterDate { get; set; }
    }
}
