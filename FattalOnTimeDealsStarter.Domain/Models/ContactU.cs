﻿using System;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class ContactU
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public short? StatusId { get; set; }
        public short? SubjectId { get; set; }
        public string SiteUrl { get; set; }
        public string FullName { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Msg { get; set; }
        public string File1 { get; set; }
        public bool? IsDivur { get; set; }
        public DateTime? Idate { get; set; }
        public string InsideTxt { get; set; }
        public string Language { get; set; }
        public string ClientIp { get; set; }
        public bool? IsPlatinum { get; set; }
    }
}
