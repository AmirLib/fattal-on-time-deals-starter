﻿using System;

#nullable disable

namespace FattalOnTimeDealsStarter.Domain.Models
{
    public partial class CartLine
    {
        public int Id { get; set; }
        public int? OrderId { get; set; }
        public DateTime? TimeStamp { get; set; }
        public double? LockedFinalPrice { get; set; }
        public int? RoomCompositionId { get; set; }
        public string SystemText { get; set; }
        public string Roomname { get; set; }
        public string LastName { get; set; }
        public int BaseId { get; set; }
        public string CartId { get; set; }
        public string UniqId { get; set; }
        public int SkyClubDiscountPrice { get; set; }
    }
}
