﻿namespace FattalOnTimeDealsStarter.Domain.Enums
{
    public enum LogLevel
    {
        Info = 1,
        Debug = 2,
        Log = 3,
        Error = 4,
        Critical = 5
    }
}
