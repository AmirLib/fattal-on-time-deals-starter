﻿namespace FattalOnTimeDealsStarter.Domain.Enums
{
    public enum LogType
    {
        Deals = 1,
        Order = 2,
        Optima = 3,
        Pelecard = 4,
        ExposeBox = 5,
        Oracle = 6,
        APILogin = 7,
        General = 8,
        Email = 9,
        NoAvailableDealsSoon = 10,
        Cache = 11,
        OptimaNoHotelReservation = 12,
        GoogleAnalytic = 13,
        DealAvailability = 14,
        OrderPayment = 15,
        OrderComplete = 16,
        CurrencyUpdater = 17
    }
}
