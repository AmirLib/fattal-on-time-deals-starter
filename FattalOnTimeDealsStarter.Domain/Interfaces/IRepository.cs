﻿namespace FattalOnTimeDealsStarter.Domain.Interfaces
{
    public interface IRepository
    {
        void SaveChanges();
    }
}
