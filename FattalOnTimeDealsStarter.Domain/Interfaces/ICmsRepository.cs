﻿using FattalOnTimeDealsStarter.Domain.Models;
using System.Collections.Generic;

namespace FattalOnTimeDealsStarter.Domain.Interfaces
{
    public interface ICmsRepository : IRepository
    {
        Cm GetNameBarById(int id);
        List<Cm> GetAllActive();
        Dictionary<int, Cm> GetAllActiveAsDictionary();
    }
}
