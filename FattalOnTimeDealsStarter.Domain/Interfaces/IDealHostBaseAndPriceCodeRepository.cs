﻿using FattalOnTimeDealsStarter.Domain.Models;
using System.Collections.Generic;
using System.Linq;

namespace FattalOnTimeDealsStarter.Domain.Interfaces
{
    public interface IDealHostBaseAndPriceCodeRepository
    {
        List<IGrouping<int, DealHostBaseAndPriceCode>> GetDealsByPrecentageGroups(HashSet<int> ids);
    }
}
