﻿using FattalOnTimeDealsStarter.Domain.Models;

namespace FattalOnTimeDealsStarter.Domain.Interfaces
{
    public interface IApplicationLogRepository: IRepository
    {
        void Add(ApplicationLog applicationLog);
    }
}
