﻿using FattalOnTimeDealsStarter.Domain.Models;
using System;
using System.Collections.Generic;

namespace FattalOnTimeDealsStarter.Domain.Interfaces
{
    public interface IAutomaticDealsDataRepository : IRepository
    {
        AutomaticDealsData Get(int hotelId, DateTime arrivalDate, DateTime deptDate);
        List<AutomaticDealsData> GetAllByNotMailSent();
        void InsertNewInformation(
            int hotelId,
            DateTime arrivalDate,
            DateTime deptDate,
            string priceCode);
        bool IsExists(int hotelId, DateTime arrivalDate, DateTime deptDate);
    }
}
